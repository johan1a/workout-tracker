addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.5.4")
// Provides dependencyUpdates
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.6.4")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "2.3.1")
addSbtPlugin("io.spray" % "sbt-revolver" % "0.10.0")
addSbtPlugin("au.com.onegeek" % "sbt-dotenv" % "2.1.233")
