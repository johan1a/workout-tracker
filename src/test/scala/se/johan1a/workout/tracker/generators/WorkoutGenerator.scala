package se.johan1a.workout.tracker.generators

import org.scalacheck.Gen
import se.johan1a.workout.tracker.generators.DateGenerators.localDateTimeGen
import se.johan1a.workout.tracker.model.{ExerciseSet, Workout, WorkoutData, WorkoutExercise}
import java.util.UUID

object WorkoutGenerator {

  val exerciseSetGenerator: Gen[ExerciseSet] = for {
    index <- Gen.choose(0, 10)
    reps <- Gen.choose(0, 10)
    weight <- Gen.choose(0.0, 150)
    durationSeconds <- Gen.choose(0, 120)
    localDateTime <- localDateTimeGen
    checkedAt <- Gen.oneOf(Some(localDateTime), None)
  } yield ExerciseSet(
    index,
    Some(reps),
    Some(weight),
    Some(durationSeconds),
    checkedAt,
    recordMarkers = None
  )

  val workoutExerciseGenerator: Gen[WorkoutExercise] = for {
    index <- Gen.choose(0, 20)
    exerciseId <- Gen.uuid
    sets <- Gen.listOf(exerciseSetGenerator)
  } yield WorkoutExercise(
    index,
    exerciseId,
    sets.sortBy(set => (set.index, set.weight))
  )

  def workoutGenerator(userId: UUID): Gen[Workout] = for {
    id <- Gen.uuid
    date <- DateGenerators.localDateGen
    exercises <- Gen.listOf(workoutExerciseGenerator)
  } yield Workout(Some(id), date, WorkoutData(exercises), programId = None, userId, 0)

  def workoutsGenerator(userId: UUID): Gen[Seq[Workout]] =
    Gen.listOf(workoutGenerator(userId))
}
