package se.johan1a.workout.tracker.generators

import org.scalacheck.Gen
import se.johan1a.workout.tracker.model.{Metric, MetricType, MetricTypeId, MetricTypeName, UserId}

import java.time.LocalDate
import java.util.UUID

object MetricGenerators {

  def metricTypeGenerator(
      userId: UserId,
      metricTypeId: MetricTypeId = MetricTypeId(UUID.randomUUID())
  ): Gen[MetricType] = for {
    name <- Gen.asciiPrintableStr
    minValue <- Gen.option(Gen.choose(-500, 20))
    maxValue <- Gen.option(Gen.choose(20, 500))
  } yield MetricType(
    Some(metricTypeId),
    userId,
    MetricTypeName(name),
    minValue,
    maxValue
  )

  def metricTypesGenerator(userId: UserId): Gen[Seq[MetricType]] = for {
    metricTypes <- Gen.listOfN(10, metricTypeGenerator(userId))
  } yield metricTypes

  def metricGenerator(metricTypeId: MetricTypeId = MetricTypeId(UUID.randomUUID())): Gen[Metric] =
    for {
      value <- Gen.choose(-100, 100)
      note <- Gen.option(Gen.asciiPrintableStr)
      epochDay <- Gen.choose(0, 20000)
    } yield Metric(
      metricTypeId,
      date = LocalDate.ofEpochDay(epochDay),
      value = value,
      note = note
    )

  def metricsGenerator(metricTypeId: MetricTypeId): Gen[Seq[Metric]] = for {
    metrics <- Gen.listOfN(20, metricGenerator(metricTypeId))
  } yield metrics

  def metricWithMetricTypeGenerator(userId: UserId): Gen[(MetricType, Metric)] = for {
    metricType <- metricTypeGenerator(userId)
    value <- Gen.choose(-100, 100)
    note <- Gen.option(Gen.asciiPrintableStr)
    epochDay <- Gen.choose(0, 20000)
  } yield {
    (
      metricType,
      Metric(
        metricType.id.get,
        date = LocalDate.ofEpochDay(epochDay),
        value = value,
        note = note
      )
    )
  }
}
