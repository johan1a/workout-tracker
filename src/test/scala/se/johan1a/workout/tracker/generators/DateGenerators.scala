package se.johan1a.workout.tracker.generators

import org.scalacheck.Gen

import java.time.temporal.ChronoUnit
import java.time.{Instant, LocalDate, LocalDateTime, ZoneOffset}

object DateGenerators {

  def localDateGen: Gen[LocalDate] = {
    for {
      dateTime <- localDateTimeGen
    } yield dateTime.toLocalDate
  }

  def localDateTimeGen: Gen[LocalDateTime] = {
    val now = Instant.now()
    val min = now.minus(5 * 12 * 30, ChronoUnit.DAYS).toEpochMilli
    val max = now.toEpochMilli
    for {
      millis <- Gen.choose(min, max)
      instant = Instant.ofEpochMilli(millis)
    } yield LocalDateTime.ofInstant(instant, ZoneOffset.UTC)
  }
}
