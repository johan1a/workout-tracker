package se.johan1a.workout.tracker.generators

import org.scalacheck.Gen
import se.johan1a.workout.tracker.model.{Exercise, ExerciseType}
import java.util.UUID

object ExerciseGenerator {

  def exerciseGenerator(userId: UUID): Gen[Exercise] = for {
    id <- Gen.uuid
    name <- Gen.asciiPrintableStr
    exerciseType <- Gen.oneOf(
      ExerciseType.reps,
      ExerciseType.duration,
      ExerciseType.repsAndWeight,
      ExerciseType.distance
    )
  } yield Exercise(Some(id), name, exerciseType, userId)

}
