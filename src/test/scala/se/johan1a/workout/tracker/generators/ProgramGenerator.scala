package se.johan1a.workout.tracker.generators

import org.scalacheck.Gen
import se.johan1a.workout.tracker.model._

import java.util.UUID

object ProgramGenerator {

  val set: Gen[ProgramSet] = for {
    index <- Gen.choose(0, 10)
    percentage <- Gen.choose(0, 10.0)
    fixedWeight <- Gen.choose(0, 100.0)
    nbrReps <- Gen.choose(0, 10)
  } yield ProgramSet(
    index = index,
    percentageOfMax = Some(percentage),
    fixedWeight = Some(fixedWeight),
    nbrReps = nbrReps
  )

  val exercise: Gen[ProgramExercise] = for {
    id <- Gen.uuid
    sets <- Gen.listOfN(5, set)
  } yield ProgramExercise(
    index = None,
    exerciseId = id,
    sets = sets.zipWithIndex.map { case (set, index) =>
      set.copy(index = index)
    }
  )

  val weeks: Gen[Seq[ProgramWeek]] = for {
    exercises <- Gen.listOfN(2, exercise)
  } yield 0
    .to(2)
    .map(weekIndex => {
      ProgramWeek(
        index = weekIndex,
        days = 0
          .to(2)
          .map(dayIndex => {
            ProgramDay(
              index = dayIndex,
              exercises = exercises.zipWithIndex.map { case (exercise, i) =>
                exercise.copy(index = Some(i))
              }
            )
          })
      )
    })

  val programData: Gen[ProgramData] = for {
    weeks <- weeks
    currentRound <- Gen.choose(0, 100)
    currentWeek <- Gen.choose(0, weeks.size - 1)
    currentDay <- Gen.choose(0, weeks(currentWeek).days.size - 1)
    offsetMode <- Gen.choose(0, 5)
    trainingMax <- Gen.choose(0, 200.0)
    trainingMaxDefaultIncrement <- Gen.choose(0, 5.0)
  } yield {
    val exercises: Seq[ProgramExercise] =
      weeks.flatMap(_.days.flatMap(_.exercises))
    val trainingMaxes = exercises.map(e => {
      TrainingMax(
        exerciseId = e.exerciseId,
        weights = Seq(trainingMax),
        defaultIncrement = trainingMaxDefaultIncrement
      )
    })

    ProgramData(
      weeks = weeks,
      trainingMaxes = trainingMaxes,
      currentRound,
      currentDay,
      currentWeek,
      offsetMode,
      Seq.empty
    )
  }

  def programGenerator(userId: UUID): Gen[Program] = for {
    id <- Gen.uuid
    programType <- Gen.choose(0, 3)
    name <- Gen.alphaNumStr
    data <- programData
  } yield Program(Some(id), programType, name, data, userId)
}
