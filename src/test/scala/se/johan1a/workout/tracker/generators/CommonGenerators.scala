package se.johan1a.workout.tracker.generators

import se.johan1a.workout.tracker.model.{MetricTypeId, UserId}

import java.util.UUID

// Non-scalacheck generators
object CommonGenerators {

  def userId(): UserId = UserId(UUID.randomUUID())

  def metricTypeId(): MetricTypeId = MetricTypeId(UUID.randomUUID())

}
