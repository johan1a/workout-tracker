package se.johan1a.workout.tracker.utils

import scala.io.Source

object TestUtils {

  def readFileAsString(filename: String): String = {
    val source = Source.fromResource(filename)
    val result = source.mkString("")
    source.close()
    result
  }

}
