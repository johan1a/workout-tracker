package se.johan1a.workout.tracker.utils

import com.dimafeng.testcontainers.{ForAllTestContainer, PostgreSQLContainer}
import org.testcontainers.utility.DockerImageName
import se.johan1a.workout.tracker.service.UnitTest

trait PostgresIntegrationTest extends UnitTest with ForAllTestContainer {

  val postgresImageName: DockerImageName = DockerImageName.parse("postgres:14.1")

  override val container: PostgreSQLContainer =
    PostgreSQLContainer(
      databaseName = "workout_tracker",
      username = "workout_tracker",
      password = "workout_tracker",
      dockerImageNameOverride = postgresImageName
    )

  override def afterStart(): Unit = {
    PostgresTestContainerContext.createTables(new PostgresTestContainerContext(container).db)
  }

}
