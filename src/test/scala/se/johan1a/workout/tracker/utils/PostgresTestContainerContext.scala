package se.johan1a.workout.tracker.utils

import com.dimafeng.testcontainers.PostgreSQLContainer
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime
import se.johan1a.workout.tracker.db.LiquibaseRunner
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.{JdbcBackend, JdbcProfile}

import scala.concurrent.Await

class PostgresTestContainerContext(container: PostgreSQLContainer) {

  val dbUsername = "testuser"
  val dbPassword = "testuserpw"

  implicit val config: Config = ConfigFactory.parseString(
    s"""
       |
       |database = {
       |
       |  profile = "slick.jdbc.PostgresProfile$$"
       |
       |  connectionPool = "HikariCP"
       |  dataSourceClass = "org.postgresql.ds.PGSimpleDataSource"
       |  properties = {
       |    serverName = ${container.host}
       |    portNumber = ${container.mappedPort(5432)}
       |    databaseName = workout_tracker
       |    user = ${container.username}
       |    password = ${container.password}
       |  }
       |  numThreads = 10
       |}
       |
       |auth.serverSecret = AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
       |
       |defaultUser {
       |  username = $dbUsername
       |  password = $dbPassword
       |}
       |""".stripMargin
  )

  val databaseConfig: DatabaseConfig[JdbcProfile] =
    DatabaseConfig.forConfig[JdbcProfile]("database", config)

  val db: JdbcBackend.DatabaseDef = Database.forConfig("database", config)

  def clearData(): Unit = {
    val tables: Seq[String] = Seq("workouts", "records", "metrics", "metric_types")
    tables.foreach { table =>
      Await.result(
        db.run(sqlu"delete from #$table cascade"),
        10.seconds
      )
    }
  }

}
object PostgresTestContainerContext {

  def createTables(db: JdbcBackend.Database): Unit = LiquibaseRunner.run(db)

}
