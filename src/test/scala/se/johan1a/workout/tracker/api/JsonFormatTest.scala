package se.johan1a.workout.tracker.api

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import se.johan1a.workout.tracker.api.JsonFormat.{
  createWorkoutRequest,
  exerciseFormat,
  metricTypeJsonFormat,
  workoutFormat
}
import se.johan1a.workout.tracker.model.request.{CreateWorkoutRequest, FromProgram}
import se.johan1a.workout.tracker.model._
import spray.json._

import java.time.LocalDate
import java.util.UUID

class JsonFormatTest extends AnyFunSpec with Matchers {

  private val userId = UUID.randomUUID()

  describe("Exercise") {

    it("should parse Exercise from string ") {
      val id = UUID.randomUUID()
      val input = s"""{
          |"id": "$id",
          |"name": "curls",
          |"exerciseType": "${ExerciseType.repsAndWeight}",
          |"userId": "$userId"
          |}""".stripMargin.parseJson
      val output = input.convertTo[Exercise]
      output shouldBe Exercise(
        id = Some(id),
        name = "curls",
        exerciseType = ExerciseType.repsAndWeight,
        userId
      )
    }

    it("should parse type reps") {
      val id = UUID.randomUUID()
      val input = s"""{
          |"id": "$id",
          |"name": "curls",
          |"exerciseType": "${ExerciseType.reps}",
          |"userId": "$userId"
          |}""".stripMargin.parseJson
      val output = input.convertTo[Exercise]
      output shouldBe Exercise(
        id = Some(id),
        name = "curls",
        exerciseType = ExerciseType.reps,
        userId
      )
    }

    it("should parse type duration") {
      val id = UUID.randomUUID()
      val input = s"""{
          |"id": "$id",
          |"name": "curls",
          |"exerciseType": "${ExerciseType.duration}",
          |"userId": "$userId"
          |}""".stripMargin.parseJson
      val output = input.convertTo[Exercise]
      output shouldBe Exercise(
        id = Some(id),
        name = "curls",
        exerciseType = ExerciseType.duration,
        userId
      )
    }

    it("should parse type distance") {
      val id = UUID.randomUUID()
      val input = s"""{
          |"id": "$id",
          |"name": "curls",
          |"exerciseType": "${ExerciseType.distance}",
          |"userId": "$userId"
          |}""".stripMargin.parseJson
      val output = input.convertTo[Exercise]
      output shouldBe Exercise(
        id = Some(id),
        name = "curls",
        exerciseType = ExerciseType.distance,
        userId
      )
    }
  }

  describe("Workout") {

    it("should parse a Workout from string ") {
      val date = LocalDate.now()
      val id = UUID.randomUUID()
      val programId = UUID.randomUUID()
      val input = s"""{
                    |"data": {"exercises": []},
                    |"id": "$id",
                    |"date": "$date",
                    |"userId": "$userId",
                    |"programId": "$programId",
                    |"version": 0
                    |}""".stripMargin.parseJson
      val output = input.convertTo[Workout]
      output shouldBe Workout(
        id = Some(id),
        date = date,
        data = WorkoutData(Seq.empty),
        programId = Some(programId),
        userId = userId,
        version = 0
      )
    }

  }

  describe("CreateWorkoutRequest") {

    it("should parse a CreateWorkoutRequest from a string") {
      val date = LocalDate.now()
      val workoutId = UUID.randomUUID()
      val programId = UUID.randomUUID()
      val input = s"""{
                     |"workoutId": "$workoutId",
                     |"programId": "$programId",
                     |"date": "$date",
                     |"method": "FromProgram"
                     |}""".stripMargin.parseJson
      val output = input.convertTo[CreateWorkoutRequest]
      output shouldBe CreateWorkoutRequest(
        workoutId = Some(workoutId),
        programId = Some(programId),
        date = date,
        method = FromProgram
      )
    }
  }

  describe("MetricType") {

    it("should parse a MetricType from a string") {
      val metricTypeId = UUID.randomUUID()
      val userId = UUID.randomUUID()
      val name = "nteyrantraotnera"
      val input =
        s""" {
           | "id": "$metricTypeId",
           | "userId": "$userId",
           | "name": "$name",
           | "maxValue": 100
           | }
           |""".stripMargin.parseJson

      val output = input.convertTo[MetricType]
      output shouldBe MetricType(
        id = Some(MetricTypeId(metricTypeId)),
        userId = UserId(userId),
        name = MetricTypeName(name),
        minValue = None,
        maxValue = Some(100)
      )
    }

    it("should parse a String into a MetricType") {
      val metricTypeId = UUID.randomUUID()
      val userId = UUID.randomUUID()
      val name = "nteyrantraotnera"
      val expected = {
        s"""{"id":"$metricTypeId","maxValue":100,"name":"$name","userId":"$userId"}""".stripMargin
      }

      val input: MetricType = MetricType(
        id = Some(MetricTypeId(metricTypeId)),
        userId = UserId(userId),
        name = MetricTypeName(name),
        minValue = None,
        maxValue = Some(100)
      )

      val output = input.toJson.toString()

      output shouldBe expected
    }
  }
}
