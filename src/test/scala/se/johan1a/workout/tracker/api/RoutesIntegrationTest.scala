package se.johan1a.workout.tracker.api

import com.typesafe.scalalogging.Logger
import org.apache.pekko.http.javadsl.model.headers.HttpCredentials
import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import org.apache.pekko.http.scaladsl.model.ContentTypes.`application/json`
import org.apache.pekko.http.scaladsl.model.headers.{Cookie, HttpCookie, `Set-Cookie`}
import org.apache.pekko.http.scaladsl.model.{HttpEntity, MediaTypes, StatusCodes}
import org.apache.pekko.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import org.apache.pekko.testkit.TestDuration
import org.scalacheck.Gen
import se.johan1a.workout.tracker.ProgramRoutes
import se.johan1a.workout.tracker.api.JsonFormat._
import se.johan1a.workout.tracker.db.RefreshTokenService
import se.johan1a.workout.tracker.generators.ExerciseGenerator.exerciseGenerator
import se.johan1a.workout.tracker.generators.WorkoutGenerator.{workoutGenerator, workoutsGenerator}
import se.johan1a.workout.tracker.model._
import se.johan1a.workout.tracker.service._
import se.johan1a.workout.tracker.utils.TestUtils.readFileAsString
import se.johan1a.workout.tracker.utils.{PostgresIntegrationTest, PostgresTestContainerContext}
import spray.json.DefaultJsonProtocol.immSeqFormat
import spray.json.enrichAny

import java.util.UUID
import scala.collection.immutable
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class RoutesIntegrationTest extends PostgresIntegrationTest with ScalatestRouteTest {

  implicit val timeout: RouteTestTimeout = RouteTestTimeout(60.seconds.dilated)

  class TestContext() extends PostgresTestContainerContext(container) {
    private implicit val logger: Logger = Logger("RoutesTest")

    implicit val executionContext: ExecutionContext = ExecutionContext.global
    val recordsService = new RecordsService(db, databaseConfig)
    val workoutService = new WorkoutService(recordsService, db, databaseConfig)
    val exerciseService = new ExerciseService(db, databaseConfig)
    val programService = new ProgramService(db, databaseConfig)
    val userService = new UserService(db, databaseConfig)
    val createWorkoutService = new CreateWorkoutServiceImpl(programService, workoutService)
    implicit val refreshTokenStorage: RefreshTokenService = new RefreshTokenService(db)
    val sessionManagement = new SessionManagement()
    val metricTypeService = new MetricTypeService(db, databaseConfig)
    val metricService = new MetricService(metricTypeService, db, databaseConfig)
    val metricRoutes = new MetricRoutes(sessionManagement, metricTypeService, metricService)
    val authRoutes = new AuthRoutes(userService, sessionManagement)
    val workoutRoutes = new WorkoutRoutes(workoutService, createWorkoutService, sessionManagement)
    val programRoutes = new ProgramRoutes(programService, sessionManagement)
    val exerciseRoutes = new ExerciseRoutes(exerciseService, sessionManagement)
    val recordRoutes = new RecordRoutes(recordsService, workoutService, sessionManagement)

    val routes = new Routes(
      metricRoutes,
      authRoutes,
      workoutRoutes,
      programRoutes,
      exerciseRoutes,
      recordRoutes
    )

    def login(): (Seq[Cookie], UUID) = {
      val username = "testuser"
      val password = "testuserpw"

      val authHeaders: Seq[Cookie] = Post("/api/auth/login").addCredentials(
        HttpCredentials.createBasicHttpCredentials(username, password)
      ) ~> routes.routes ~> check {
        responseAs[String] shouldEqual "OK"
        val cookies = response.headers.collect { case `Set-Cookie`(cookies) => cookies }
        val sessionData: HttpCookie = cookies.find(_.name == "_sessiondata").get
        immutable.Seq(Cookie("_sessiondata", sessionData.value))
      }

      val userId = Get("/api/auth/user").withHeaders(authHeaders) ~> routes.routes ~> check {
        responseAs[UserResponse].user.id
      }
      (authHeaders, userId)
    }
  }

  var authHeaders: Seq[Cookie] = Seq.empty
  var userId: UUID = UUID.randomUUID()

  lazy val t = new TestContext()

  override def afterStart(): Unit = {
    PostgresTestContainerContext.createTables(t.db)

    t.userService.createDefaultUser().futureValue
    val (headers: Seq[Cookie], id: UUID) = t.login()
    authHeaders = headers
    userId = id
  }

  describe("Exercises") {
    it("should list empty exercises") {
      Get("/api/exercises/").withHeaders(
        authHeaders
      ) ~> t.routes.routes ~> check {
        responseAs[ExerciseListResponse] shouldEqual ExerciseListResponse(
          Seq()
        )
      }
    }

    it("can save an exercise") {
      forAll(exerciseGenerator(userId)) { exercise: Exercise =>
        val string = exercise.toJson.toString
        Post(
          "/api/exercises",
          HttpEntity(`application/json`, string)
            .withContentType(`application/json`)
        ).withHeaders(authHeaders) ~> t.routes.routes ~> check {
          mediaType shouldEqual MediaTypes.`application/json`
          responseAs[Exercise].name shouldEqual exercise.name
          responseAs[Exercise].exerciseType shouldEqual exercise.exerciseType
        }
      }
    }

    it("can update an exercise") {
      forAll(exerciseGenerator(userId)) { exercise: Exercise =>

        Post(
          "/api/exercises",
          HttpEntity(`application/json`, exercise.toJson.toString)
            .withContentType(`application/json`)
        ).withHeaders(authHeaders) ~> t.routes.routes ~> check {
          mediaType shouldEqual MediaTypes.`application/json`
          responseAs[Exercise].name shouldEqual exercise.name
          responseAs[Exercise].exerciseType shouldEqual exercise.exerciseType
        }

        val updatedExercise = exercise.copy(name = "something else")

        Post(
          "/api/exercises",
          HttpEntity(`application/json`, updatedExercise.toJson.toString)
            .withContentType(`application/json`)
        ).withHeaders(authHeaders) ~> t.routes.routes ~> check {
          mediaType shouldEqual MediaTypes.`application/json`
          responseAs[Exercise].name shouldEqual updatedExercise.name
          responseAs[Exercise].exerciseType shouldEqual exercise.exerciseType
        }
      }
    }
  }

  describe("Workouts") {
    it("should list empty workouts") {
      Get("/api/workouts/").withHeaders(
        authHeaders
      ) ~> t.routes.routes ~> check {
        responseAs[WorkoutListResponse] shouldEqual WorkoutListResponse(
          Seq()
        )
      }
    }

    it("should save workouts") {
      forAll(workoutGenerator(userId)) { workout =>
        t.clearData()
        Post(
          "/api/workouts",
          HttpEntity(
            `application/json`,
            workout.copy(id = None).toJson.toString
          )
            .withContentType(`application/json`)
        ).withHeaders(
          authHeaders
        ) ~> t.routes.routes ~> check {
          status shouldEqual StatusCodes.OK
          mediaType shouldEqual MediaTypes.`application/json`
          val savedWorkout = responseAs[Workout]
          savedWorkout.id.isDefined shouldBe true
          savedWorkout.date shouldBe workout.date
          val savedExercises = savedWorkout.data.exercises
          savedExercises.map(e => (e.index, e.exerciseId)) shouldBe workout.data.exercises.map(e =>
            (e.index, e.exerciseId)
          )

          val savedSets = savedExercises.map(e => e.sets)
          val expectedSets = workout.data.exercises.map(e => e.sets)
          savedSets shouldBe expectedSets

          savedWorkout.version shouldBe workout.version
        }
      }
    }

    it("should throw an exception on version mismatch") {
      forAll(workoutGenerator(userId)) { workout =>
        t.clearData()

        var id: Option[UUID] = None
        Post(
          "/api/workouts",
          HttpEntity(
            `application/json`,
            workout.copy(id = None).toJson.toString
          )
            .withContentType(`application/json`)
        ).withHeaders(
          authHeaders
        ) ~> t.routes.routes ~> check {
          status shouldEqual StatusCodes.OK
          mediaType shouldEqual MediaTypes.`application/json`
          val savedWorkout = responseAs[Workout]
          savedWorkout.id.isDefined shouldBe true
          id = savedWorkout.id
        }

        Post(
          "/api/workouts",
          HttpEntity(
            `application/json`,
            workout.copy(id = id, version = workout.version + 2).toJson.toString
          )
            .withContentType(`application/json`)
        ).withHeaders(
          authHeaders
        ) ~> t.routes.routes ~> check {
          status shouldEqual StatusCodes.BadRequest
          mediaType shouldEqual MediaTypes.`text/plain`
          responseAs[
            String
          ] shouldEqual "Version mismatch, expected 1 but got 2"
        }
      }
    }

    it("should list all workouts") {
      forAll(workoutsGenerator(userId)) { workouts =>
        t.clearData()

        Future
          .sequence(workouts.map(workout => {
            t.workoutService.save(UUID.randomUUID(), workout.copy(id = None))
          }))
          .futureValue

        Get("/api/workouts/").withHeaders(
          authHeaders
        ) ~> t.routes.routes ~> check {
          val response = responseAs[WorkoutListResponse].workouts
          response.size shouldBe workouts.size
          response
            .map(_.date) shouldBe response.sortBy(_.date).reverse.map(_.date)
        }
      }
    }

    it("should return workouts of given size") {
      forAll(workoutsGenerator(userId), Gen.choose(0, Integer.MAX_VALUE)) { case (workouts, size) =>
        t.clearData()

        Future
          .sequence(workouts.map(workout => {
            t.workoutService
              .save(UUID.randomUUID(), workout.copy(id = None))
          }))
          .futureValue

        Get(s"/api/workouts/?size=$size").withHeaders(
          authHeaders
        ) ~> t.routes.routes ~> check {
          val response = responseAs[WorkoutListResponse].workouts
          response.size shouldBe Math.min(size, workouts.size)
          response.map(_.date) shouldBe response
            .sortBy(_.date)
            .reverse
            .map(_.date)
        }
      }
    }

    it("should offset workouts of given size") {
      forAll(workoutsGenerator(userId), Gen.choose(0, 50)) { case (workouts, offset) =>
        val size = 5
        t.clearData()

        Future
          .sequence(workouts.map(workout => {
            t.workoutService
              .save(UUID.randomUUID(), workout.copy(id = None))
          }))
          .futureValue

        Get(
          s"/api/workouts/?size=$size&offset=$offset"
        ).withHeaders(authHeaders) ~> t.routes.routes ~> check {
          val response = responseAs[WorkoutListResponse].workouts
          response.size shouldBe Math.min(
            size,
            Math.max(workouts.size - offset, 0)
          )

          response.map(_.date) shouldBe workouts
            .sortBy(_.date)
            .reverse
            .drop(offset)
            .take(size)
            .map(_.date)
        }
      }
    }
  }

  describe("Programs") {
    it("should list empty programs") {
      Get("/api/programs/").withHeaders(
        authHeaders
      ) ~> t.routes.routes ~> check {
        responseAs[ProgramListResponse] shouldEqual ProgramListResponse(
          Seq()
        )
      }
    }

    it("should update a program") {
      val body = readFileAsString("testdata/upsert_program_command_0.json").replace(
        "USER_ID",
        userId.toString
      )

      Post(
        "/api/programs",
        HttpEntity(`application/json`, body).withContentType(`application/json`)
      ).withHeaders(authHeaders) ~> t.routes.routes ~> check {
        status shouldBe StatusCodes.OK
        mediaType shouldEqual MediaTypes.`application/json`
        val program = responseAs[Program]
        program.name shouldEqual "531"
        val weeks = program.data.weeks
        weeks.size shouldEqual 3
      }
    }

    it("should create a new program without specifying indices") {
      val body = readFileAsString("testdata/create_program_command_0.json")

      Post(
        "/api/programs",
        HttpEntity(`application/json`, body).withContentType(`application/json`)
      ).withHeaders(authHeaders) ~> t.routes.routes ~> check {
        status shouldBe StatusCodes.OK
        mediaType shouldEqual MediaTypes.`application/json`
        val program = responseAs[Program]
        program.name shouldEqual "New program"

        val weeks = program.data.weeks
        weeks.size shouldEqual 3
        weeks.map(_.index) shouldEqual Seq(0, 1, 2)

        val days = weeks.head.days
        days.map(_.index) shouldEqual Seq(0, 1, 2)

        val exercise = days.head.exercises.head
        val sets = exercise.sets
        sets.map(_.index) shouldBe Seq(0, 1, 2, 3, 4, 5, 6, 7, 8)
      }
    }
  }

  describe("Records") {
    it("should list empty records") {
      t.clearData()

      Get("/api/records/").withHeaders(
        authHeaders
      ) ~> t.routes.routes ~> check {
        val response = responseAs[RecordsResponse]
        response.records.exerciseRecords shouldBe Seq.empty
      }
    }

    it("should update records when saving a workout") {
      forAll(workoutGenerator(userId)) { workout: Workout =>
        t.clearData()

        Post(
          "/api/workouts",
          HttpEntity(
            `application/json`,
            workout.copy(id = None).toJson.toString
          )
            .withContentType(`application/json`)
        ).withHeaders(authHeaders) ~> t.routes.routes ~> check {
          mediaType shouldEqual MediaTypes.`application/json`
          val response = responseAs[Workout]
          response.date shouldEqual workout.date
        }

        val heaviestSets: Map[UUID, ExerciseSet] = workout.data.exercises
          .map(exercise => {
            val sorted =
              exercise.sets.filter(_.checkedAt.isDefined).sortBy(_.weight)
            exercise.exerciseId -> sorted.lastOption
          })
          .collect { case id -> Some(weight) =>
            id -> weight
          }
          .toMap

        Get("/api/records").withHeaders(
          authHeaders
        ) ~> t.routes.routes ~> check {
          val response = responseAs[RecordsResponse]
          if (workout.data.exercises.nonEmpty) {
            heaviestSets.map { case (id, set) =>
              val recordWeights = response.records.exerciseRecords
                .find(record => record.exerciseId == id)
                .map(_.repMaxes.map(_.weight))
                .get
              recordWeights should contain(set.weight.get)
            }
          }
        }
      }
    }

    it("should recalulate records") {
      Post("/api/records/recalculate").withHeaders(
        authHeaders
      ) ~> t.routes.routes ~> check {
        responseAs[String] shouldBe "OK"
      }
    }
  }

  describe("Auth") {
    it("should be able to login") {
      val username = "testuser"
      val password = "testuserpw"

      Post("/api/auth/login").addCredentials(
        HttpCredentials.createBasicHttpCredentials(username, password)
      ) ~> t.routes.routes ~> check {
        responseAs[String] shouldEqual "OK"
      }
    }
  }

  describe("MetricTypes") {
    it("should return not found if no MetricType was found") {
      Get("/api/metric-types").withHeaders(
        authHeaders
      ) ~> t.routes.routes ~> check {
        val response = responseAs[Seq[MetricType]]

        response shouldBe empty
      }
    }

  }

}
