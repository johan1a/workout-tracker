package se.johan1a.workout.tracker.api

import org.apache.pekko.http.javadsl.model.headers.Authorization
import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import org.apache.pekko.http.scaladsl.model.ContentTypes.`application/json`
import org.apache.pekko.http.scaladsl.model.headers.{
  Cookie,
  HttpChallenge,
  HttpCookie,
  `Set-Cookie`
}
import org.apache.pekko.http.scaladsl.model.{HttpEntity, StatusCodes}
import org.apache.pekko.http.scaladsl.server.{
  AuthenticationFailedRejection,
  AuthorizationFailedRejection
}
import org.apache.pekko.http.scaladsl.testkit.ScalatestRouteTest
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.Logger
import org.scalacheck.Gen
import se.johan1a.workout.tracker.api.JsonFormat._
import se.johan1a.workout.tracker.generators.ExerciseGenerator._
import se.johan1a.workout.tracker.generators.ProgramGenerator._
import se.johan1a.workout.tracker.generators.WorkoutGenerator.workoutGenerator
import se.johan1a.workout.tracker.generators.{
  CommonGenerators,
  MetricGenerators,
  ProgramGenerator,
  WorkoutGenerator
}
import se.johan1a.workout.tracker.model._
import se.johan1a.workout.tracker.model.request.{
  CreateUserRequest,
  CreateWorkoutRequest,
  FromProgram
}
import se.johan1a.workout.tracker.service._
import spray.json.DefaultJsonProtocol.immSeqFormat
import spray.json.enrichAny

import java.time.LocalDate
import java.util.UUID
import scala.concurrent.Future
import com.softwaremill.pekkohttpsession.InMemoryRefreshTokenStorage
import se.johan1a.workout.tracker.auth.UserSession
import com.softwaremill.pekkohttpsession.RefreshTokenStorage
import se.johan1a.workout.tracker.ProgramRoutes
import se.johan1a.workout.tracker.api.dto.UpsertProgramCommand

class RoutesTest extends UnitTest with ScalatestRouteTest {

  class TestContext {

    private implicit val logger: Logger = Logger("RoutesTest")
    private implicit val config: Config = ConfigFactory.parseString(
      s"""
       |
       |database = {
       |
       |  profile = "slick.jdbc.PostgresProfile$$"
       |
       |  connectionPool = "HikariCP"
       |  dataSourceClass = "org.postgresql.ds.PGSimpleDataSource"
       |  numThreads = 10
       |}
       |auth.serverSecret = AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
       |""".stripMargin
    )

    val workoutService: WorkoutService = stub[WorkoutService]
    val exerciseService: ExerciseService = stub[ExerciseService]
    val programService: ProgramService = stub[ProgramService]
    private val recordsService: RecordsService = stub[RecordsService]
    val userService: UserService = stub[UserService]
    val createWorkoutService: CreateWorkoutService = stub[CreateWorkoutService]

    implicit val refreshTokenStorage
        : RefreshTokenStorage[se.johan1a.workout.tracker.auth.UserSession] =
      new InMemoryRefreshTokenStorage[UserSession]() {
        override def log(msg: String) = logger.info(msg)
      }
    val sessionManagement: SessionManagement = new SessionManagement()
    val metricTypeService: MetricTypeService = stub[MetricTypeService]
    val metricService: MetricService = stub[MetricService]
    private val metricRoutes = new MetricRoutes(sessionManagement, metricTypeService, metricService)
    private val authRoutes = new AuthRoutes(userService, sessionManagement)
    val workoutRoutes = new WorkoutRoutes(workoutService, createWorkoutService, sessionManagement)
    val programRoutes = new ProgramRoutes(programService, sessionManagement)
    val exerciseRoutes = new ExerciseRoutes(exerciseService, sessionManagement)
    val recordRoutes = new RecordRoutes(recordsService, workoutService, sessionManagement)

    val routes =
      new Routes(
        metricRoutes,
        authRoutes,
        workoutRoutes,
        programRoutes,
        exerciseRoutes,
        recordRoutes
      )
  }

  describe("Login") {
    it("Should not be able to get /api/auth/user when logged out") {
      val t = new TestContext()

      Get("/api/auth/user") ~> t.routes.routes ~> check {
        rejection shouldEqual AuthorizationFailedRejection
      }
    }

    it("Should be able to login") {
      val t = new TestContext()

      (t.userService.authenticate _)
        .when(*, *)
        .returning(
          Future.successful(Some(User(Some(UUID.randomUUID()), "", "")))
        )

      val headers = Seq(Authorization.basic("theUser", "thePassword"))
      Post("/api/auth/login").withHeaders(headers) ~> t.routes.routes ~> check {
        status shouldBe StatusCodes.OK
      }
    }

    it("Should be rejected on wrong password") {
      val t = new TestContext()

      (t.userService.authenticate _)
        .when(*, *)
        .returning(
          Future.successful(None)
        )

      val headers = Seq(Authorization.basic("theUser", "thePassword"))
      Post("/api/auth/login").withHeaders(headers) ~> t.routes.routes ~> check {
        rejection shouldEqual AuthenticationFailedRejection(
          AuthenticationFailedRejection.CredentialsRejected,
          HttpChallenge("Basic", "secure site", Map("charset" -> "UTF-8"))
        )
      }
    }

    it("Should be able to get user endpoint when logged in") {
      val t = new TestContext()
      val userId = UUID.randomUUID()
      val username = "the username"
      val loggedInHeaders = login(t.userService, t.routes, UserId(userId), username = username)

      Get("/api/auth/user").withHeaders(
        loggedInHeaders
      ) ~> t.routes.routes ~> check {
        responseAs[UserResponse] shouldEqual UserResponse(
          UserResponseUser(
            userId,
            username
          )
        )
      }
    }

    describe("logged in user") {
      val t = new TestContext()
      val userId = UUID.randomUUID()
      val loggedInHeaders = login(t.userService, t.routes, UserId(userId))

      it("should fetch own workout") {
        forAll(workoutGenerator(userId)) { workout =>

          (t.workoutService.fetch _)
            .when(workout.id.get)
            .returning {
              Future.successful(Some(workout))
            }

          Get(s"/api/workouts/${workout.id.get}").withHeaders(
            loggedInHeaders
          ) ~> t.routes.routes ~> check {
            val response = responseAs[Workout]
            response shouldBe workout
          }
        }
      }

      it("should not fetch another users workout") {
        forAll(workoutGenerator(userId)) { workout =>
          (t.workoutService.fetch _)
            .when(workout.id.get)
            .returning {
              Future.successful(Some(workout.copy(userId = UUID.randomUUID())))
            }

          Get(s"/api/workouts/${workout.id.get}").withHeaders(
            loggedInHeaders
          ) ~> t.routes.routes ~> check {
            status shouldEqual StatusCodes.Forbidden
          }
        }
      }

      it("should not save another users workout") {
        forAll(workoutGenerator(userId)) { workout =>
          val body = workout.copy(userId = UUID.randomUUID()).toJson.toString

          Post(
            s"/api/workouts",
            HttpEntity(
              `application/json`,
              body
            ).withContentType(`application/json`)
          ).withHeaders(
            loggedInHeaders
          ) ~> t.routes.routes ~> check {
            status shouldEqual StatusCodes.Forbidden
          }
        }
      }

      it("should not fetch another users exercise") {
        forAll(exerciseGenerator(userId)) { exercise =>
          (t.exerciseService.fetch _)
            .when(exercise.id.get)
            .returning {
              Future.successful(Some(exercise.copy(userId = UUID.randomUUID())))
            }

          Get(s"/api/exercises/${exercise.id.get}").withHeaders(
            loggedInHeaders
          ) ~> t.routes.routes ~> check {
            status shouldEqual StatusCodes.Forbidden
          }
        }
      }

      it("should not save another users exercise") {
        forAll(exerciseGenerator(userId)) { exercise =>
          val body = exercise.copy(userId = UUID.randomUUID()).toJson.toString

          Post(
            s"/api/exercises",
            HttpEntity(
              `application/json`,
              body
            ).withContentType(`application/json`)
          ).withHeaders(
            loggedInHeaders
          ) ~> t.routes.routes ~> check {
            status shouldEqual StatusCodes.Forbidden
          }
        }
      }

      it("should not fetch another users program") {
        forAll(programGenerator(userId)) { program =>
          (t.programService.fetch _)
            .when(program.id.get)
            .returning {
              Future.successful(Some(program.copy(userId = UUID.randomUUID())))
            }

          Get(s"/api/programs/${program.id.get}").withHeaders(
            loggedInHeaders
          ) ~> t.routes.routes ~> check {
            status shouldEqual StatusCodes.Forbidden
          }
        }
      }

      it("should save the program for the logged in user") {
        forAll(programGenerator(userId)) { program =>
          val body = program.copy(userId = UUID.randomUUID()).toJson.toString

          (t.programService
            .save(_: UpsertProgramCommand, _: UserId))
            .when(*, UserId(userId))
            .returning(Future.successful(program))

          Post(
            s"/api/programs",
            HttpEntity(
              `application/json`,
              body
            ).withContentType(`application/json`)
          ).withHeaders(
            loggedInHeaders
          ) ~> t.routes.routes ~> check {
            status shouldEqual StatusCodes.OK
            responseAs[Program].userId shouldBe userId
          }
        }
      }

      it("should create a new user") {
        forAll(Gen.alphaNumStr, Gen.alphaNumStr) { (username, password) =>
          (t.userService.create _)
            .when(*, *)
            .returning {
              Future.successful(
                User(Some(UUID.randomUUID()), username, "hashed password")
              )
            }

          val body: String =
            CreateUserRequest(username, password).toJson.toString

          Post(
            s"/api/auth/users",
            HttpEntity(
              `application/json`,
              body
            ).withContentType(`application/json`)
          ).withHeaders(loggedInHeaders) ~> t.routes.routes ~> check {
            status shouldEqual StatusCodes.Created
          }
        }
      }

      it("should be able to log out") {
        Post(
          s"/api/auth/logout"
        ).withHeaders(loggedInHeaders) ~> t.routes.routes ~> check {
          status shouldEqual StatusCodes.OK
          val cookie = response.headers
            .find(_.name() == "Set-Cookie")
            .map(_.asInstanceOf[`Set-Cookie`].cookie)
          cookie.find(_.name == "_sessiondata").map(_.value) shouldBe Some(
            "deleted"
          )
        }
      }
    }
  }

  describe("Workouts") {
    val userId = UUID.randomUUID()

    it("should create a workout based on a program") {
      forAll(
        ProgramGenerator.programGenerator(userId),
        WorkoutGenerator.workoutGenerator(userId)
      ) { case (program, workout) =>
        val t = new TestContext()
        (t.createWorkoutService.createWorkout _)
          .when(*, *)
          .returning(Future.successful(workout))

        val body: String = CreateWorkoutRequest(
          date = LocalDate.now().plusWeeks(2),
          method = FromProgram,
          programId = program.id,
          workoutId = None
        ).toJson.toString

        Post(
          s"/api/workouts/create",
          HttpEntity(
            `application/json`,
            body
          ).withContentType(`application/json`)
        ).withHeaders(login(t.userService, t.routes)) ~> t.routes.routes ~> check {
          status shouldEqual StatusCodes.Created
          val response = responseAs[Workout]
          response shouldBe workout
        }
      }
    }
  }

  describe("MetricTypes") {

    it("should list empty MetricTypes") {
      val t = new TestContext()
      val loggedInHeaders = login(t.userService, t.routes)
      (t.metricTypeService.list _).when(*).returning(Future.successful(Seq.empty))

      Get(s"/api/metric-types/").withHeaders(
        loggedInHeaders
      ) ~> t.routes.routes ~> check {
        val response = responseAs[Seq[MetricType]]
        response shouldBe Seq.empty
      }
    }

    it("should list MetricTypes") {
      val userId = CommonGenerators.userId()
      forAll(MetricGenerators.metricTypesGenerator(userId)) { metricTypes =>
        val t = new TestContext()
        val loggedInHeaders = login(t.userService, t.routes, userId)
        (t.metricTypeService.list _).when(*).returning(Future.successful(metricTypes))

        Get(s"/api/metric-types/").withHeaders(loggedInHeaders) ~> t.routes.routes ~> check {
          val response = responseAs[Seq[MetricType]]
          response shouldBe metricTypes
        }
      }
    }

    it("should fetch MetricTypes") {
      val userId = CommonGenerators.userId()
      forAll(MetricGenerators.metricTypeGenerator(userId)) { metricType =>
        val t = new TestContext()
        val loggedInHeaders = login(t.userService, t.routes, userId)
        (t.metricTypeService.fetch _).when(*, *).returning(Future.successful(Some(metricType)))

        Get(s"/api/metric-types/${metricType.id.get.value}")
          .withHeaders(loggedInHeaders) ~> t.routes.routes ~> check {
          val response = responseAs[MetricType]
          response shouldBe metricType
        }
      }
    }

    it("should upsert MetricTypes") {
      val userId = CommonGenerators.userId()
      forAll(MetricGenerators.metricTypeGenerator(userId)) { metricType =>
        val t = new TestContext()
        val loggedInHeaders = login(t.userService, t.routes, userId)
        (t.metricTypeService.upsert _).when(*, *).onCall { (_: UserId, metricType: MetricType) =>
          Future.successful(metricType)
        }

        Post(
          s"/api/metric-types/",
          HttpEntity(`application/json`, metricType.toJson.toString)
        ).withHeaders(loggedInHeaders) ~> t.routes.routes ~> check {
          val response = responseAs[MetricType]
          response shouldBe metricType
        }
      }
    }

    it("should delete MetricTypes") {
      val userId = CommonGenerators.userId()
      forAll(MetricGenerators.metricTypeGenerator(userId)) { metricType =>
        val t = new TestContext()
        val loggedInHeaders = login(t.userService, t.routes, userId)
        (t.metricTypeService.delete _).when(*, *).onCall { (_: UserId, _: MetricTypeId) =>
          Future.successful(())
        }

        Delete(
          s"/api/metric-types/${metricType.id.get.value}"
        ).withHeaders(loggedInHeaders) ~> t.routes.routes ~> check {
          println(response)
          status shouldBe StatusCodes.OK
        }
      }
    }
  }

  describe("Metrics") {

    it("should list empty Metrics") {
      val t = new TestContext()
      val loggedInHeaders = login(t.userService, t.routes)
      (t.metricService.list _).when(*, *).returning(Future.successful(Seq.empty))

      Get(s"/api/metric-types/fae86242-22da-4625-af21-52cd602d5544/metrics").withHeaders(
        loggedInHeaders
      ) ~> t.routes.routes ~> check {
        val response = responseAs[Seq[Metric]]
        response shouldBe Seq.empty
      }
    }

    it("should list Metrics") {
      val userId = CommonGenerators.userId()
      val metricTypeId = MetricTypeId(UUID.randomUUID())

      forAll(
        MetricGenerators.metricTypeGenerator(userId, metricTypeId),
        MetricGenerators.metricsGenerator(metricTypeId)
      ) { (metricType, metrics) =>
        val t = new TestContext()
        val loggedInHeaders = login(t.userService, t.routes, userId)
        (t.metricService.list _).when(*, *).returning(Future.successful(metrics))

        Get(s"/api/metric-types/${metricType.id.get.value}/metrics").withHeaders(
          loggedInHeaders
        ) ~> t.routes.routes ~> check {
          val response = responseAs[Seq[Metric]]
          response shouldBe metrics
        }
      }
    }

    it("should upsert Metrics") {
      val userId = CommonGenerators.userId()
      forAll(MetricGenerators.metricGenerator()) { metric =>
        val t = new TestContext()
        val loggedInHeaders = login(t.userService, t.routes, userId)

        (t.metricService.upsert _).when(*, *).onCall { (_: UserId, metric: Metric) =>
          Future.successful(metric)
        }

        Post(
          s"/api/metric-types/c32698ab-835d-4a00-b75f-cb1af161fedb/metrics",
          HttpEntity(`application/json`, metric.toJson.toString)
        ).withHeaders(loggedInHeaders) ~> t.routes.routes ~> check {
          val response = responseAs[Metric]
          response shouldBe metric
        }
      }
    }

    it("should delete Metrics") {
      val userId = CommonGenerators.userId()
      forAll(MetricGenerators.metricGenerator()) { metric =>
        val t = new TestContext()

        (t.metricService.delete _).when(*, *, *).onCall {
          (_: UserId, _: MetricTypeId, _: LocalDate) =>
            Future.successful(())
        }

        val loggedInHeaders = login(t.userService, t.routes, userId)
        Delete(
          s"/api/metric-types/c32698ab-835d-4a00-b75f-cb1af161fedb/metrics",
          HttpEntity(`application/json`, metric.toJson.toString)
        ).withHeaders(loggedInHeaders) ~> t.routes.routes ~> check {
          status shouldBe StatusCodes.OK
        }
      }
    }
  }

  def login(
      userService: UserService,
      routes: Routes,
      userId: UserId = CommonGenerators.userId(),
      username: String = "some username"
  ): Seq[Cookie] = {
    (userService.authenticate _)
      .when(*, *)
      .returning(
        Future.successful(Some(User(Some(userId.value), username, "")))
      )
    val loginHeaders = Seq(Authorization.basic(username, "thePassword"))

    var cookie: Option[HttpCookie] = None
    Post("/api/auth/login").withHeaders(
      loginHeaders
    ) ~> routes.routes ~> check {
      status shouldBe StatusCodes.OK
      cookie = response.headers
        .find(_.name() == "Set-Cookie")
        .map(_.asInstanceOf[`Set-Cookie`].cookie)
    }
    Seq(Cookie(cookie.get.name, cookie.get.value))
  }

}
