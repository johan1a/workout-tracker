package se.johan1a.workout.tracker.service

import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatest.concurrent.{Eventually, ScalaFutures}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

trait UnitTest
    extends AnyFunSpec
    with Matchers
    with MockFactory
    with Eventually
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with ScalaFutures
    with ScalaCheckPropertyChecks {

  implicit val patience: PatienceConfig = PatienceConfig(scaled(5.seconds), scaled(100.millis))

}
