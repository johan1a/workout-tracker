package se.johan1a.workout.tracker.service

import com.dimafeng.testcontainers.ForAllTestContainer
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.pekko.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import org.apache.pekko.testkit.TestDuration
import org.scalamock.scalatest.MockFactory
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.{Eventually, ScalaFutures}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Span}
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import se.johan1a.workout.tracker.model._
import se.johan1a.workout.tracker.utils.PostgresIntegrationTest
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.{JdbcBackend, JdbcProfile}

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID
import scala.concurrent.Await
import scala.concurrent.duration._

class RecordsServiceIntegrationTest
    extends AnyFunSpec
    with Matchers
    with ScalatestRouteTest
    with MockFactory
    with ForAllTestContainer
    with Eventually
    with BeforeAndAfterEach
    with ScalaCheckPropertyChecks
    with ScalaFutures
    with PostgresIntegrationTest {

  override implicit val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1000, Millis)))

  implicit val timeout: RouteTestTimeout = RouteTestTimeout(60.seconds.dilated)

  var configOpt: Option[Config] = None

  private var recordsService: Option[RecordsService] = None
  private val defaultUserName = "default u"
  private val defaultUserPassword = "default p"
  private val userId = UUID.randomUUID()

  var db: Option[JdbcBackend.DatabaseDef] = None

  override def beforeAll(): Unit = {
    implicit val config: Config = ConfigFactory.parseString(
      s"""
         |
         |database = {
         |
         |  profile = "slick.jdbc.PostgresProfile$$"
         |
         |  connectionPool = "HikariCP"
         |  dataSourceClass = "org.postgresql.ds.PGSimpleDataSource"
         |  properties = {
         |    serverName = ${container.host}
         |    portNumber = ${container.mappedPort(5432)}
         |    databaseName = workout_tracker
         |    user = ${container.username}
         |    password = ${container.password}
         |  }
         |  numThreads = 10
         |}
         |defaultUser {
         |  username = $defaultUserName
         |  password = $defaultUserPassword
         |}
         |auth.serverSecret = AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
         |
    """.stripMargin
    )
    configOpt = Some(config)

    val databaseConfig: DatabaseConfig[JdbcProfile] =
      DatabaseConfig.forConfig[JdbcProfile]("database", configOpt.get)

    db = Some(Database.forConfig("database", configOpt.get))

    recordsService = Some(new RecordsService(db.get, databaseConfig))
  }

  override def beforeEach(): Unit = {
    removeData(db.get)
  }

  it("should be a record when going from unchecked to checked") {
    val exerciseId = UUID.randomUUID()
    val set = ExerciseSet(
      0,
      Some(1),
      Some(3),
      None,
      None,
      None
    )
    val workout = Workout(
      None,
      LocalDate.now(),
      WorkoutData(
        Seq(
          WorkoutExercise(
            0,
            exerciseId,
            Seq(set)
          )
        )
      ),
      userId = userId,
      version = 0,
      programId = None
    )

    val records0 =
      recordsService.get.saveNewRecords(userId, workout).futureValue
    records0.exerciseRecords shouldBe Seq(
      ExerciseRecordData(exerciseId, Seq.empty)
    )

    val updatedWorkout = workout.copy(data =
      workout.data.copy(exercises =
        Seq(
          WorkoutExercise(
            0,
            exerciseId,
            Seq(
              ExerciseSet(
                0,
                Some(1),
                Some(3),
                None,
                Some(LocalDateTime.now()),
                None
              )
            )
          )
        )
      )
    )

    val records1 =
      recordsService.get.saveNewRecords(userId, updatedWorkout).futureValue
    records1.exerciseRecords shouldBe Seq(
      ExerciseRecordData(exerciseId, Seq(RepMax(1, 3, workout.date, 0)))
    )
  }

  it("should not consider sets where weight is 0") {
    val exerciseId = UUID.randomUUID()
    val set = ExerciseSet(
      0,
      Some(1),
      Some(0),
      None,
      Some(LocalDateTime.now),
      None
    )
    val workout = Workout(
      None,
      LocalDate.now(),
      WorkoutData(
        Seq(
          WorkoutExercise(
            0,
            exerciseId,
            Seq(set)
          )
        )
      ),
      userId = userId,
      version = 0,
      programId = None
    )

    val savedWorkout =
      recordsService.get.saveNewRecords(userId, workout).futureValue

    savedWorkout.exerciseRecords shouldBe Seq(
      ExerciseRecordData(exerciseId, Seq.empty)
    )
  }

  it("should not count the same record twice") {
    val exerciseId = UUID.randomUUID()

    val workout = Workout(
      None,
      LocalDate.now(),
      WorkoutData(
        Seq(
          WorkoutExercise(
            0,
            exerciseId,
            Seq(
              ExerciseSet(
                0,
                Some(1),
                Some(10),
                None,
                Some(LocalDateTime.now),
                None
              ),
              ExerciseSet(
                1,
                Some(1),
                Some(10),
                None,
                Some(LocalDateTime.now.plusMinutes(1)),
                None
              )
            )
          )
        )
      ),
      userId = userId,
      version = 0,
      programId = None
    )

    val savedWorkout =
      recordsService.get.saveNewRecords(userId, workout).futureValue

    savedWorkout.exerciseRecords shouldBe Seq(
      ExerciseRecordData(exerciseId, Seq(RepMax(1, 10, workout.date, 0)))
    )
  }

  it("should remove outdated saved records when there is a new one") {

    val exerciseId = UUID.randomUUID()
    val set = ExerciseSet(
      0,
      Some(1),
      Some(50),
      None,
      Some(LocalDateTime.now()),
      None
    )
    val workout0 = Workout(
      None,
      LocalDate.now(),
      WorkoutData(
        Seq(
          WorkoutExercise(
            0,
            exerciseId,
            Seq(set)
          )
        )
      ),
      userId = userId,
      version = 0,
      programId = None
    )

    val expected0 =
      Seq(ExerciseRecordData(exerciseId, Seq(RepMax(1, 50, workout0.date, 0))))

    val savedWorkout0 =
      recordsService.get.saveNewRecords(userId, workout0).futureValue
    savedWorkout0.exerciseRecords shouldBe expected0

    recordsService.get
      .fetch(userId)
      .futureValue
      .exerciseRecords shouldBe expected0

    val workout1 = workout0.copy(
      date = LocalDate.now().plusDays(1),
      data = workout0.data.copy(exercises =
        Seq(
          WorkoutExercise(
            0,
            exerciseId,
            Seq(
              ExerciseSet(
                0,
                Some(1),
                Some(52.5),
                None,
                Some(LocalDateTime.now()),
                None
              )
            )
          )
        )
      )
    )

    val expected1 =
      Seq(
        ExerciseRecordData(exerciseId, Seq(RepMax(1, 52.5, workout1.date, 0)))
      )

    val savedWorkout1 =
      recordsService.get.saveNewRecords(userId, workout1).futureValue
    savedWorkout1.exerciseRecords shouldBe expected1

    recordsService.get
      .fetch(userId)
      .futureValue
      .exerciseRecords shouldBe expected1
  }

  it(
    "should not remove valid records for same exercise when nbr reps is different"
  ) {

    val exerciseId = UUID.randomUUID()
    val set = ExerciseSet(
      0,
      Some(2),
      Some(50),
      None,
      Some(LocalDateTime.now()),
      None
    )
    val workout0 = Workout(
      None,
      LocalDate.now(),
      WorkoutData(
        Seq(
          WorkoutExercise(
            0,
            exerciseId,
            Seq(set)
          )
        )
      ),
      userId = userId,
      version = 0,
      programId = None
    )

    val expected0 =
      Seq(ExerciseRecordData(exerciseId, Seq(RepMax(2, 50, workout0.date, 0))))

    val savedWorkout0 =
      recordsService.get.saveNewRecords(userId, workout0).futureValue
    savedWorkout0.exerciseRecords shouldBe expected0

    recordsService.get
      .fetch(userId)
      .futureValue
      .exerciseRecords shouldBe expected0

    val workout1 = workout0.copy(
      date = LocalDate.now().plusDays(1),
      data = workout0.data.copy(exercises =
        Seq(
          WorkoutExercise(
            0,
            exerciseId,
            Seq(
              ExerciseSet(
                0,
                Some(2),
                Some(40),
                None,
                Some(LocalDateTime.now()),
                None
              ),
              ExerciseSet(
                0,
                Some(1),
                Some(52.5),
                None,
                Some(LocalDateTime.now()),
                None
              )
            )
          )
        )
      )
    )

    val expected1 =
      Seq(
        ExerciseRecordData(
          exerciseId,
          Seq(
            RepMax(2, 50, workout0.date, 0),
            RepMax(1, 52.5, workout1.date, 0)
          )
        )
      )

    val savedWorkout1 =
      recordsService.get.saveNewRecords(userId, workout1).futureValue
    savedWorkout1.exerciseRecords shouldBe expected1

    recordsService.get
      .fetch(userId)
      .futureValue
      .exerciseRecords shouldBe expected1
  }

  private def removeData(db: JdbcBackend.Database) = {
    Await.result(
      db.run(sqlu"truncate records"),
      3.seconds
    )
  }

}
