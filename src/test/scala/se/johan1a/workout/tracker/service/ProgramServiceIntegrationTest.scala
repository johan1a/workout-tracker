package se.johan1a.workout.tracker.service;

import com.dimafeng.testcontainers.PostgreSQLContainer
import com.typesafe.scalalogging.Logger
import se.johan1a.workout.tracker.generators.ProgramGenerator
import se.johan1a.workout.tracker.model.{User, UserId}
import se.johan1a.workout.tracker.utils.{PostgresIntegrationTest, PostgresTestContainerContext}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.ExecutionContext
import scala.io.Source

class ProgramServiceIntegrationTest extends PostgresIntegrationTest {

  class PostgresTestContext(container: PostgreSQLContainer)
      extends PostgresTestContainerContext(container) {
    implicit val executionContext: ExecutionContext = ExecutionContext.global
    implicit val logger: Logger = Logger("PostgresTestContext")

    val programService = new ProgramService(db, databaseConfig)
    val userService = new UserService(db, databaseConfig)
  }

  lazy val t = new PostgresTestContext(container)
  lazy val user: User = t.userService.createDefaultUser().futureValue
  lazy val userId: UserId = UserId(user.id.get)

  override def afterStart(): Unit = {
    PostgresTestContainerContext.createTables(t.db)
  }

  it("should set exercise indices if non exist in the database") {
    val program = ProgramGenerator.programGenerator(userId.value).sample.get.copy(id = None)
    val savedProgram = t.programService.save(program).futureValue

    val data: String =
      Source.fromResource("testdata/program_data_without_exercise_indices.json").mkString
    t.db.run(sqlu"update programs set data = $data").futureValue

    val fetchedProgram = t.programService.fetch(savedProgram.id.get).futureValue

    val exercises = fetchedProgram.map(_.data.weeks.head.days.head.exercises)
    exercises.isDefined.shouldBe(true)

    val expected = Seq(Some(0), Some(1))
    exercises.get.map(_.index).shouldBe(expected)
  }

}
