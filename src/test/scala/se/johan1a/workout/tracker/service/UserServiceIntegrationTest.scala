package se.johan1a.workout.tracker.service

import com.dimafeng.testcontainers.ForAllTestContainer
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.Logger
import org.apache.pekko.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import org.apache.pekko.testkit.TestDuration
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import se.johan1a.workout.tracker.utils.PostgresIntegrationTest
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.{JdbcBackend, JdbcProfile}

import scala.concurrent.Await
import scala.concurrent.duration._

class UserServiceIntegrationTest
    extends AnyFunSpec
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with ForAllTestContainer
    with ScalaFutures
    with ScalaCheckPropertyChecks
    with PostgresIntegrationTest {

  override implicit val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = scaled(Span(1000, Millis)))

  implicit val timeout: RouteTestTimeout = RouteTestTimeout(60.seconds.dilated)

  private implicit val logger: Logger = Logger("RoutesTest")

  var configOpt: Option[Config] = None

  private var userService: Option[UserService] = None
  private val defaultUserName = "default u"
  private val defaultUserPassword = "default p"

  var db: Option[JdbcBackend.DatabaseDef] = None

  override def beforeAll(): Unit = {
    implicit val config: Config = ConfigFactory.parseString(
      s"""
         |
         |database = {
         |
         |  profile = "slick.jdbc.PostgresProfile$$"
         |
         |  connectionPool = "HikariCP"
         |  dataSourceClass = "org.postgresql.ds.PGSimpleDataSource"
         |  properties = {
         |    serverName = ${container.host}
         |    portNumber = ${container.mappedPort(5432)}
         |    databaseName = workout_tracker
         |    user = ${container.username}
         |    password = ${container.password}
         |  }
         |  numThreads = 10
         |}
         |defaultUser {
         |  username = $defaultUserName
         |  password = $defaultUserPassword
         |}
         |auth.serverSecret = AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
         |
    """.stripMargin
    )
    configOpt = Some(config)

    val databaseConfig: DatabaseConfig[JdbcProfile] =
      DatabaseConfig.forConfig[JdbcProfile]("database", configOpt.get)

    db = Some(Database.forConfig("database", configOpt.get))

    userService = Some(new UserService(db.get, databaseConfig))
  }

  override def beforeEach(): Unit = {
    removeData(db.get)
  }

  describe("create()") {

    it("should return a user if it succeeded") {
      val username = "username"
      val result =
        userService.get.create(username, "password 12345").futureValue
      result.username shouldBe username
      result.id.isDefined shouldBe true
      result.hashedPassword.nonEmpty shouldBe true
    }

  }

  describe("findByUsername()") {

    it("should return None when the user is not found") {
      userService.get.findByUsername("XXX").futureValue shouldBe None
    }

    it("should return the User when found") {
      val username = "username"
      userService.get.create(username, "password 12345").futureValue

      val result = userService.get.findByUsername(username).futureValue.get
      result.username shouldBe username
      result.id.isDefined shouldBe true
      result.hashedPassword.nonEmpty shouldBe true
    }
  }

  describe("authenticate()") {

    it("should return None if it could not authenticate") {
      val result = userService.get.authenticate("nterai", "ntuyra").futureValue
      result shouldBe None
    }

    it("should return the user if it could authenticate") {
      val username = "the username"
      val password = "password 12345678"
      userService.get.create(username, password).futureValue

      val result =
        userService.get.authenticate(username, password).futureValue.get
      result.username shouldBe username
      result.id.isDefined shouldBe true
      result.hashedPassword.nonEmpty shouldBe true
    }

  }

  describe("createDefaultUser()") {
    it("should create a default user if it does not exist") {
      userService.get
        .findByUsername(defaultUserName)
        .futureValue shouldBe None

      userService.get.createDefaultUser().futureValue

      val result =
        userService.get.findByUsername(defaultUserName).futureValue.get
      result.username shouldBe defaultUserName
      result.id.isDefined shouldBe true
      result.hashedPassword.nonEmpty shouldBe true
    }

    it("should not create default user if it exists") {
      val user0 = userService.get.createDefaultUser().futureValue

      val user1 = userService.get.createDefaultUser().futureValue

      user0 shouldBe user1
    }
  }

  private def removeData(db: JdbcBackend.Database) = {
    Await.result(
      db.run(sqlu"truncate users cascade"),
      3.seconds
    )
  }

}
