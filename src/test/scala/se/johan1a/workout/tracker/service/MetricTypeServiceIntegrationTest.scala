package se.johan1a.workout.tracker.service

import com.dimafeng.testcontainers.PostgreSQLContainer
import com.typesafe.scalalogging.Logger
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import se.johan1a.workout.tracker.generators.MetricGenerators
import se.johan1a.workout.tracker.model.{MetricTypeName, User, UserId}
import se.johan1a.workout.tracker.utils.{PostgresIntegrationTest, PostgresTestContainerContext}

import scala.concurrent.ExecutionContext

class MetricTypeServiceIntegrationTest
    extends PostgresIntegrationTest
    with ScalaCheckPropertyChecks {

  class PostgresTestContext(container: PostgreSQLContainer)
      extends PostgresTestContainerContext(container) {
    implicit val executionContext: ExecutionContext = ExecutionContext.global
    implicit val logger: Logger = Logger("MetricTypeServiceIntegrationTest")

    val metricTypeService = new MetricTypeService(db, databaseConfig)
    val userService = new UserService(db, databaseConfig)
  }

  lazy val t = new PostgresTestContext(container)
  lazy val user: User = t.userService.createDefaultUser().futureValue
  lazy val userId: UserId = UserId(user.id.get)

  override def afterStart(): Unit = {
    PostgresTestContainerContext.createTables(t.db)
  }

  it("should be able to upsert MetricTypes") {
    t.clearData()

    forAll(MetricGenerators.metricTypeGenerator(userId)) { metricType =>

      val result = t.metricTypeService.upsert(userId, metricType).futureValue

      result shouldBe metricType
    }
  }

  it("should be able to fetch MetricTypes") {
    t.clearData()

    forAll(MetricGenerators.metricTypeGenerator(userId)) { metricType =>

      val savedMetricType =
        t.metricTypeService.upsert(userId, metricType.copy(id = None)).futureValue

      val result = t.metricTypeService.fetch(userId, savedMetricType.id.get).futureValue

      result shouldBe Some(savedMetricType)
    }
  }

  it("should be able to update MetricTypes") {
    t.clearData()

    forAll(MetricGenerators.metricTypeGenerator(userId)) { metricType =>

      val savedMetricType =
        t.metricTypeService.upsert(userId, metricType.copy(id = None)).futureValue

      val updatedMetricType = savedMetricType.copy(name = MetricTypeName("new name"))

      t.metricTypeService.upsert(userId, updatedMetricType).futureValue

      val result = t.metricTypeService.fetch(userId, savedMetricType.id.get).futureValue

      result shouldBe Some(updatedMetricType)
    }
  }

  it("should be able to list MetricTypes") {
    forAll(MetricGenerators.metricTypesGenerator(userId)) { metricTypes =>
      t.clearData()

      val savedMetricTypes = metricTypes.map(m => {
        t.metricTypeService.upsert(userId, m.copy(id = None)).futureValue
      })

      val result = t.metricTypeService.list(userId).futureValue

      result shouldBe savedMetricTypes
    }
  }

  it("should be able to delete MetricTypes") {
    t.clearData()

    forAll(MetricGenerators.metricTypeGenerator(userId)) { metricType =>

      val savedMetricType =
        t.metricTypeService.upsert(userId, metricType.copy(id = None)).futureValue

      t.metricTypeService.delete(userId, savedMetricType.id.get).futureValue

      val result = t.metricTypeService.fetch(userId, savedMetricType.id.get).futureValue

      result shouldBe None
    }
  }
}
