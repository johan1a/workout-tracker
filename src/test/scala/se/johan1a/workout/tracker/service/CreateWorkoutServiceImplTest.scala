package se.johan1a.workout.tracker.service

import org.scalamock.matchers.ArgCapture.CaptureOne
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import se.johan1a.workout.tracker.generators.ProgramGenerator.programGenerator
import se.johan1a.workout.tracker.generators.WorkoutGenerator
import se.johan1a.workout.tracker.model._
import se.johan1a.workout.tracker.model.request.{
  CopyWorkout,
  CreateWorkoutRequest,
  FromProgram,
  New
}

import java.time.LocalDate
import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}

class CreateWorkoutServiceImplTest extends UnitTest with ScalaCheckPropertyChecks {

  private implicit val ec: ExecutionContext = ExecutionContext.global
  private val programService = stub[ProgramService]
  private val workoutService = stub[WorkoutService]

  val createWorkoutService =
    new CreateWorkoutServiceImpl(programService, workoutService)

  it("should create a new workout") {
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )

    val userId = UUID.randomUUID()
    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = New,
      programId = None,
      workoutId = None
    )
    val workout =
      createWorkoutService.createWorkout(userId, request).futureValue

    workout.id.isDefined shouldBe true
    workout.version shouldBe 0
    workout.userId shouldBe userId
  }

  it("should create a workout from a program") {
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )

    val userId = UUID.randomUUID()

    forAll(programGenerator(userId)) { program =>
      (programService.fetch _)
        .when(*)
        .returning(Future.successful(Some(program)))

      val savedProgram = CaptureOne[Program]()
      (programService
        .save(_: Program))
        .when(capture(savedProgram))
        .once()
        .returning(Future.successful(program))

      val request = CreateWorkoutRequest(
        date = LocalDate.now(),
        method = FromProgram,
        programId = program.id,
        workoutId = None
      )
      val workout =
        createWorkoutService.createWorkout(userId, request).futureValue

      workout.id.isDefined shouldBe true
      workout.version shouldBe 0
      workout.userId shouldBe userId
      savedProgram.value.data.createdWorkoutIds shouldBe Seq(workout.id.get)
    }
  }

  it("should set weights based on training maxes") {
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )

    val userId = UUID.randomUUID()
    val exerciseId0 = UUID.randomUUID()
    val exerciseId1 = UUID.randomUUID()

    val program = defaultProgram(userId, exerciseId0, exerciseId1)
    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = FromProgram,
      programId = program.id,
      workoutId = None
    )

    (programService.fetch _).when(*).returning(Future.successful(Some(program)))
    (programService
      .save(_: Program))
      .when(*)
      .returning(Future.successful(program))

    val workout =
      createWorkoutService.createWorkout(userId, request).futureValue

    workout.data.exercises shouldBe Seq(
      WorkoutExercise(
        0,
        exerciseId0,
        Seq(
          ExerciseSet(0, Some(5), Some(80), None, None, None),
          ExerciseSet(1, Some(10), Some(90), None, None, None)
        )
      ),
      WorkoutExercise(
        1,
        exerciseId1,
        Seq(
          ExerciseSet(0, Some(5), Some(20), None, None, None)
        )
      )
    )
  }

  it("should copy supplementary exercises from the previous workout") {
    val userId = UUID.randomUUID()
    val exerciseId0 = UUID.randomUUID()
    val exerciseId1 = UUID.randomUUID()
    val supplementaryExerciseId = UUID.randomUUID()
    val previousWorkoutId = UUID.randomUUID()

    var program = defaultProgram(userId, exerciseId0, exerciseId1)
    program = program.copy(data = program.data.copy(createdWorkoutIds = Seq(previousWorkoutId)))

    val previousWorkout = Workout(
      id = Some(previousWorkoutId),
      date = LocalDate.now().minusDays(1),
      data = WorkoutData(
        Seq(
          WorkoutExercise(
            index = 0,
            exerciseId = exerciseId0,
            sets = Seq(
              ExerciseSet(
                index = 0,
                reps = Some(5),
                weight = Some(80),
                durationSeconds = None,
                checkedAt = None,
                recordMarkers = None
              )
            )
          ),
          WorkoutExercise(
            index = 1,
            exerciseId = supplementaryExerciseId,
            sets = Seq(
              ExerciseSet(
                index = 0,
                reps = Some(15),
                weight = Some(20),
                durationSeconds = None,
                checkedAt = None,
                recordMarkers = None
              )
            )
          )
        )
      ),
      userId = userId,
      version = 0,
      programId = None
    )
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )
    (workoutService.fetch _)
      .when(previousWorkoutId)
      .returning(Future.successful(Some(previousWorkout)))

    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = FromProgram,
      programId = program.id,
      workoutId = None
    )

    (programService.fetch _).when(*).returning(Future.successful(Some(program)))
    (programService
      .save(_: Program))
      .when(*)
      .returning(Future.successful(program))

    val workout =
      createWorkoutService.createWorkout(userId, request).futureValue

    workout.data.exercises shouldBe Seq(
      WorkoutExercise(
        0,
        exerciseId0,
        Seq(
          ExerciseSet(0, Some(5), Some(80), None, None, None),
          ExerciseSet(1, Some(10), Some(90), None, None, None)
        )
      ),
      WorkoutExercise(
        1,
        exerciseId1,
        Seq(
          ExerciseSet(0, Some(5), Some(20), None, None, None)
        )
      ),
      WorkoutExercise(
        2,
        supplementaryExerciseId,
        Seq(
          ExerciseSet(0, Some(15), Some(20), None, None, None)
        )
      )
    )
  }

  it("should not copy supplementary exercises for custom programs") {
    val userId = UUID.randomUUID()
    val exerciseId0 = UUID.randomUUID()
    val exerciseId1 = UUID.randomUUID()
    val supplementaryExerciseId = UUID.randomUUID()
    val previousWorkoutId = UUID.randomUUID()

    var program = defaultProgram(userId, exerciseId0, exerciseId1)
    program = program.copy(
      data = program.data.copy(createdWorkoutIds = Seq(previousWorkoutId)),
      programType = Program.CUSTOM
    )

    val previousWorkout = Workout(
      id = Some(previousWorkoutId),
      date = LocalDate.now().minusDays(1),
      data = WorkoutData(
        Seq(
          WorkoutExercise(
            index = 0,
            exerciseId = exerciseId0,
            sets = Seq(
              ExerciseSet(
                index = 0,
                reps = Some(5),
                weight = Some(80),
                durationSeconds = None,
                checkedAt = None,
                recordMarkers = None
              )
            )
          ),
          WorkoutExercise(
            index = 1,
            exerciseId = supplementaryExerciseId,
            sets = Seq(
              ExerciseSet(
                index = 0,
                reps = Some(15),
                weight = Some(20),
                durationSeconds = None,
                checkedAt = None,
                recordMarkers = None
              )
            )
          )
        )
      ),
      userId = userId,
      version = 0,
      programId = None
    )
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )
    (workoutService.fetch _)
      .when(previousWorkoutId)
      .returning(Future.successful(Some(previousWorkout)))

    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = FromProgram,
      programId = program.id,
      workoutId = None
    )

    (programService.fetch _).when(*).returning(Future.successful(Some(program)))
    (programService
      .save(_: Program))
      .when(*)
      .returning(Future.successful(program))

    val workout =
      createWorkoutService.createWorkout(userId, request).futureValue

    workout.data.exercises shouldBe Seq(
      WorkoutExercise(
        0,
        exerciseId0,
        Seq(
          ExerciseSet(0, Some(5), Some(80), None, None, None),
          ExerciseSet(1, Some(10), Some(90), None, None, None)
        )
      ),
      WorkoutExercise(
        1,
        exerciseId1,
        Seq(
          ExerciseSet(0, Some(5), Some(20), None, None, None)
        )
      )
    )
  }

  it("should use correct day for offset mode 1, odd round number") {
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )

    val userId = UUID.randomUUID()
    val exerciseId0 = UUID.randomUUID()
    val exerciseId1 = UUID.randomUUID()

    var program = defaultProgram(userId, exerciseId0, exerciseId1)
    program = program.copy(data = program.data.copy(currentRound = 1, currentWeek = 1))

    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = FromProgram,
      programId = program.id,
      workoutId = None
    )

    (programService.fetch _).when(*).returning(Future.successful(Some(program)))
    (programService
      .save(_: Program))
      .when(*)
      .returning(Future.successful(program))

    val workout =
      createWorkoutService.createWorkout(userId, request).futureValue

    workout.data.exercises shouldBe Seq(
      WorkoutExercise(
        0,
        exerciseId0,
        Seq(
          ExerciseSet(0, Some(3), Some(102.5), None, None, None)
        )
      )
    )
  }

  it("should use correct day for offset mode 1, odd round number, last day") {
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )

    val userId = UUID.randomUUID()
    val exerciseId0 = UUID.randomUUID()
    val exerciseId1 = UUID.randomUUID()

    var program = defaultProgram(userId, exerciseId0, exerciseId1)
    program =
      program.copy(data = program.data.copy(currentRound = 1, currentWeek = 1, currentDay = 1))

    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = FromProgram,
      programId = program.id,
      workoutId = None
    )

    (programService.fetch _).when(*).returning(Future.successful(Some(program)))
    (programService
      .save(_: Program))
      .when(*)
      .returning(Future.successful(program))

    val workout =
      createWorkoutService.createWorkout(userId, request).futureValue

    workout.data.exercises shouldBe Seq(
      WorkoutExercise(
        0,
        exerciseId0,
        Seq(
          ExerciseSet(0, Some(3), Some(110), None, None, None)
        )
      )
    )
  }

  it("should increment current day of the program") {
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )

    val userId = UUID.randomUUID()
    val program = defaultProgram(userId)

    (programService.fetch _)
      .when(*)
      .returning(Future.successful(Some(program)))

    val captor = new CaptureOne[Program]()
    (programService
      .save(_: Program))
      .when(capture(captor))
      .returning(Future.successful(program))

    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = FromProgram,
      programId = program.id,
      workoutId = None
    )

    createWorkoutService.createWorkout(userId, request).futureValue

    val savedProgram = captor.value

    savedProgram.data.currentDay shouldBe program.data.currentDay + 1
    savedProgram.data.currentWeek shouldBe program.data.currentWeek
  }

  it("should increment current week of the program") {
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )

    val userId = UUID.randomUUID()
    var program = defaultProgram(userId)
    program =
      program.copy(data = program.data.copy(currentDay = program.data.weeks.head.days.size - 1))

    (programService.fetch _)
      .when(*)
      .returning(Future.successful(Some(program)))

    val captor = new CaptureOne[Program]()
    (programService
      .save(_: Program))
      .when(capture(captor))
      .returning(Future.successful(program))

    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = FromProgram,
      programId = program.id,
      workoutId = None
    )

    createWorkoutService.createWorkout(userId, request).futureValue

    val savedProgram = captor.value

    savedProgram.data.currentDay shouldBe 0
    savedProgram.data.currentWeek shouldBe program.data.currentWeek + 1
  }

  it("should increment current round of the program") {
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )

    val userId = UUID.randomUUID()
    var program = defaultProgram(userId)
    val data = program.data
    program = program.copy(data =
      data.copy(
        currentDay = data.weeks.last.days.size - 1,
        currentWeek = data.weeks.size - 1
      )
    )

    (programService.fetch _)
      .when(*)
      .returning(Future.successful(Some(program)))

    val captor = new CaptureOne[Program]()
    (programService
      .save(_: Program))
      .when(capture(captor))
      .returning(Future.successful(program))

    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = FromProgram,
      programId = program.id,
      workoutId = None
    )

    createWorkoutService.createWorkout(userId, request).futureValue

    val savedProgram = captor.value

    savedProgram.data.currentDay shouldBe 0
    savedProgram.data.currentWeek shouldBe 0
    savedProgram.data.currentRound shouldBe data.currentRound + 1
  }

  it("should increment training maxes after every round") {
    (workoutService.save _)
      .when(*, *)
      .onCall((_: UUID, workout: Workout) =>
        Future.successful(workout.copy(id = Some(UUID.randomUUID())))
      )

    val userId = UUID.randomUUID()
    val exerciseId0 = UUID.randomUUID()
    val exerciseId1 = UUID.randomUUID()
    var program = defaultProgram(userId, exerciseId0, exerciseId1)
    val data = program.data
    program = program.copy(data =
      data.copy(
        currentDay = data.weeks.last.days.size - 1,
        currentWeek = data.weeks.size - 1
      )
    )

    (programService.fetch _)
      .when(*)
      .returning(Future.successful(Some(program)))

    val captor = new CaptureOne[Program]()
    (programService
      .save(_: Program))
      .when(capture(captor))
      .returning(Future.successful(program))

    val request = CreateWorkoutRequest(
      date = LocalDate.now(),
      method = FromProgram,
      programId = program.id,
      workoutId = None
    )

    createWorkoutService.createWorkout(userId, request).futureValue

    val savedProgram = captor.value

    savedProgram.data.currentRound shouldBe data.currentRound + 1
    savedProgram.data.trainingMaxes shouldBe Seq(
      TrainingMax(
        exerciseId = exerciseId0,
        weights = Seq(120, 122.5),
        defaultIncrement = 2.5
      ),
      TrainingMax(
        exerciseId = exerciseId1,
        weights = Seq(100, 105),
        defaultIncrement = 5
      )
    )
  }

  it("should copy a workout") {
    val userId = UUID.randomUUID()
    forAll(WorkoutGenerator.workoutGenerator(userId)) { workout =>
      val workoutService = stub[WorkoutService]
      (workoutService.fetch _)
        .when(*)
        .returning(Future.successful(Some(workout)))
      val newWorkoutId = UUID.randomUUID()
      (workoutService.save _)
        .when(*, *)
        .onCall((_: UUID, workout: Workout) =>
          Future.successful(workout.copy(id = Some(newWorkoutId)))
        )

      val request = CreateWorkoutRequest(
        date = LocalDate.now().plusDays(1),
        method = CopyWorkout,
        programId = None,
        workoutId = workout.id
      )
      val createdWorkout =
        new CreateWorkoutServiceImpl(programService, workoutService)
          .createWorkout(userId, request)
          .futureValue

      val expected = workout.copy(date = request.date, id = Some(newWorkoutId))

      createdWorkout shouldBe expected
    }
  }

  private def defaultProgram(
      userId: UUID,
      exerciseId0: UUID = UUID.randomUUID(),
      exerciseId1: UUID = UUID.randomUUID()
  ) = {
    Program(
      id = Some(UUID.randomUUID()),
      programType = Program.TYPE_531_FOR_BEGINNERS,
      name = "",
      data = ProgramData(
        weeks = Seq(
          ProgramWeek(
            index = 0,
            days = Seq(
              ProgramDay(
                index = 0,
                exercises = Seq(
                  ProgramExercise(
                    index = Some(0),
                    exerciseId = exerciseId0,
                    sets = Seq(
                      ProgramSet(
                        index = 0,
                        percentageOfMax = Some(65),
                        fixedWeight = None,
                        nbrReps = 5
                      ),
                      ProgramSet(
                        index = 1,
                        percentageOfMax = Some(75),
                        fixedWeight = None,
                        nbrReps = 10
                      )
                    )
                  ),
                  ProgramExercise(
                    index = Some(1),
                    exerciseId = exerciseId1,
                    sets = Seq(
                      ProgramSet(
                        index = 0,
                        percentageOfMax = None,
                        fixedWeight = Some(20),
                        nbrReps = 5
                      )
                    )
                  )
                )
              ),
              ProgramDay(
                index = 1,
                exercises = Seq(
                  ProgramExercise(
                    index = Some(0),
                    exerciseId = exerciseId1,
                    sets = Seq(
                      ProgramSet(
                        index = 0,
                        percentageOfMax = None,
                        fixedWeight = Some(20),
                        nbrReps = 5
                      )
                    )
                  )
                )
              )
            )
          ),
          ProgramWeek(
            index = 1,
            days = Seq(
              ProgramDay(
                index = 0,
                exercises = Seq(
                  ProgramExercise(
                    index = Some(0),
                    exerciseId = exerciseId0,
                    sets = Seq(
                      ProgramSet(
                        index = 0,
                        percentageOfMax = Some(80),
                        fixedWeight = None,
                        nbrReps = 3
                      )
                    )
                  )
                )
              ),
              ProgramDay(
                index = 0,
                exercises = Seq(
                  ProgramExercise(
                    index = Some(0),
                    exerciseId = exerciseId0,
                    sets = Seq(
                      ProgramSet(
                        index = 0,
                        percentageOfMax = Some(85),
                        fixedWeight = None,
                        nbrReps = 3
                      )
                    )
                  )
                )
              ),
              ProgramDay(
                index = 0,
                exercises = Seq(
                  ProgramExercise(
                    index = Some(0),
                    exerciseId = exerciseId0,
                    sets = Seq(
                      ProgramSet(
                        index = 0,
                        percentageOfMax = Some(90),
                        fixedWeight = None,
                        nbrReps = 3
                      )
                    )
                  )
                )
              )
            )
          )
        ),
        trainingMaxes = Seq(
          TrainingMax(
            exerciseId = exerciseId0,
            weights = Seq(120),
            defaultIncrement = 2.5
          ),
          TrainingMax(
            exerciseId = exerciseId1,
            weights = Seq(100),
            defaultIncrement = 5
          )
        ),
        currentRound = 0,
        currentDay = 0,
        currentWeek = 0,
        offsetMode = OffsetMode.ONE,
        createdWorkoutIds = Seq.empty
      ),
      userId = userId
    )
  }

}
