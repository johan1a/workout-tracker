package se.johan1a.workout.tracker.service

import com.dimafeng.testcontainers.PostgreSQLContainer
import com.typesafe.scalalogging.Logger
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import se.johan1a.workout.tracker.generators.{CommonGenerators, MetricGenerators}
import se.johan1a.workout.tracker.model.{User, UserId}
import se.johan1a.workout.tracker.utils.{PostgresIntegrationTest, PostgresTestContainerContext}

import scala.concurrent.ExecutionContext

class MetricServiceIntegrationTest extends PostgresIntegrationTest with ScalaCheckPropertyChecks {

  class PostgresTestContext(container: PostgreSQLContainer)
      extends PostgresTestContainerContext(container) {
    implicit val executionContext: ExecutionContext = ExecutionContext.global
    implicit val logger: Logger = Logger("PostgresTestContext")

    val metricTypeService = new MetricTypeService(db, databaseConfig)
    val metricService = new MetricService(metricTypeService, db, databaseConfig)
    val userService = new UserService(db, databaseConfig)
  }

  lazy val t = new PostgresTestContext(container)
  lazy val user: User = t.userService.createDefaultUser().futureValue
  lazy val userId: UserId = UserId(user.id.get)

  override def afterStart(): Unit = {
    PostgresTestContainerContext.createTables(t.db)
  }

  it("should be able to upsert Metrics") {
    forAll(MetricGenerators.metricTypeGenerator(userId), MetricGenerators.metricGenerator()) {
      case (metricType, metric) =>
        val savedMetricType =
          t.metricTypeService.upsert(userId, metricType.copy(id = None)).futureValue

        val metricToSave = metric.copy(metricTypeId = savedMetricType.id.get)
        val result = t.metricService.upsert(userId, metricToSave).futureValue

        result shouldBe metricToSave
    }
  }

  it("should be able to fetch Metrics") {
    forAll(MetricGenerators.metricTypeGenerator(userId), MetricGenerators.metricGenerator()) {
      case (metricType, metric) =>
        val savedMetricType =
          t.metricTypeService.upsert(userId, metricType.copy(id = None)).futureValue

        val metricToSave = metric.copy(metricTypeId = savedMetricType.id.get)
        val savedMetric = t.metricService.upsert(userId, metricToSave).futureValue

        val result =
          t.metricService.fetch(userId, savedMetric.metricTypeId, metric.date).futureValue

        result shouldBe Some(metricToSave)
    }
  }

  it("should be able to update Metrics") {
    forAll(MetricGenerators.metricTypeGenerator(userId), MetricGenerators.metricGenerator()) {
      case (metricType, metric) =>
        val savedMetricType =
          t.metricTypeService.upsert(userId, metricType.copy(id = None)).futureValue

        val metricToSave = metric.copy(metricTypeId = savedMetricType.id.get)
        val savedMetric = t.metricService.upsert(userId, metricToSave).futureValue

        val updatedMetric = savedMetric.copy(value = 666)
        t.metricService.upsert(userId, updatedMetric).futureValue

        val result =
          t.metricService.fetch(userId, savedMetric.metricTypeId, metric.date).futureValue

        result shouldBe Some(updatedMetric)
    }
  }

  it("should be able to list Metrics") {
    forAll(
      MetricGenerators.metricTypeGenerator(userId),
      MetricGenerators.metricsGenerator(CommonGenerators.metricTypeId())
    ) { case (metricType, metrics) =>
      val savedMetricType =
        t.metricTypeService.upsert(userId, metricType.copy(id = None)).futureValue

      val savedMetrics = metrics.map { m =>
        val metricToSave = m.copy(metricTypeId = savedMetricType.id.get)
        t.metricService.upsert(userId, metricToSave).futureValue
      }

      val result = t.metricService.list(userId, savedMetricType.id.get).futureValue

      val expected = savedMetrics.sortBy(_.date)

      result shouldBe expected
    }
  }

  it("should be able to delete Metrics") {
    forAll(MetricGenerators.metricTypeGenerator(userId), MetricGenerators.metricGenerator()) {
      case (metricType, metric) =>
        val savedMetricType =
          t.metricTypeService.upsert(userId, metricType.copy(id = None)).futureValue

        val metricToSave = metric.copy(metricTypeId = savedMetricType.id.get)
        val savedMetric = t.metricService.upsert(userId, metricToSave).futureValue

        t.metricService.delete(userId, savedMetric.metricTypeId, metric.date).futureValue

        val result =
          t.metricService.fetch(userId, savedMetric.metricTypeId, metric.date).futureValue

        result shouldBe None
    }
  }

}
