--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: exercises; Type: TABLE; Schema: public; Owner: workout_tracker
--

CREATE TABLE public.exercises (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text,
    exercise_type text DEFAULT 'repsAndWeight'::text,
    user_id uuid DEFAULT '199d275c-727d-49d5-89d7-25b70a8e1004'::uuid NOT NULL
);


ALTER TABLE public.exercises OWNER TO workout_tracker;

--
-- Name: metric_types; Type: TABLE; Schema: public; Owner: workout_tracker
--

CREATE TABLE public.metric_types (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    name text NOT NULL,
    min_value integer,
    max_value integer
);


ALTER TABLE public.metric_types OWNER TO workout_tracker;

--
-- Name: metrics; Type: TABLE; Schema: public; Owner: workout_tracker
--

CREATE TABLE public.metrics (
    metric_type_id uuid NOT NULL,
    date date NOT NULL,
    value integer NOT NULL,
    note text
);


ALTER TABLE public.metrics OWNER TO workout_tracker;

--
-- Name: programs; Type: TABLE; Schema: public; Owner: workout_tracker
--

CREATE TABLE public.programs (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    program_type integer,
    name text,
    data text,
    user_id uuid DEFAULT '199d275c-727d-49d5-89d7-25b70a8e1004'::uuid NOT NULL
);


ALTER TABLE public.programs OWNER TO workout_tracker;

--
-- Name: records; Type: TABLE; Schema: public; Owner: workout_tracker
--

CREATE TABLE public.records (
    user_id uuid NOT NULL,
    exercise_records text
);


ALTER TABLE public.records OWNER TO workout_tracker;

--
-- Name: refresh_token_data; Type: TABLE; Schema: public; Owner: workout_tracker
--

CREATE TABLE public.refresh_token_data (
    selector text NOT NULL,
    user_id uuid NOT NULL,
    username text NOT NULL,
    token_hash text NOT NULL,
    expires timestamp without time zone NOT NULL
);


ALTER TABLE public.refresh_token_data OWNER TO workout_tracker;

--
-- Name: users; Type: TABLE; Schema: public; Owner: workout_tracker
--

CREATE TABLE public.users (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    username text,
    hashed_password text
);


ALTER TABLE public.users OWNER TO workout_tracker;

--
-- Name: workouts; Type: TABLE; Schema: public; Owner: workout_tracker
--

CREATE TABLE public.workouts (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    date date,
    data text,
    user_id uuid DEFAULT '199d275c-727d-49d5-89d7-25b70a8e1004'::uuid NOT NULL,
    version integer DEFAULT 0 NOT NULL,
    program_id uuid
);


ALTER TABLE public.workouts OWNER TO workout_tracker;

--
-- Name: exercises exercises_pkey; Type: CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.exercises
    ADD CONSTRAINT exercises_pkey PRIMARY KEY (id);


--
-- Name: metric_types metric_types_pkey; Type: CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.metric_types
    ADD CONSTRAINT metric_types_pkey PRIMARY KEY (id);


--
-- Name: programs programs_pkey; Type: CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.programs
    ADD CONSTRAINT programs_pkey PRIMARY KEY (id);


--
-- Name: records records_pkey; Type: CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.records
    ADD CONSTRAINT records_pkey PRIMARY KEY (user_id);


--
-- Name: refresh_token_data refresh_token_data_pkey; Type: CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.refresh_token_data
    ADD CONSTRAINT refresh_token_data_pkey PRIMARY KEY (selector);


--
-- Name: metrics unique_type_date; Type: CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.metrics
    ADD CONSTRAINT unique_type_date UNIQUE (metric_type_id, date);


--
-- Name: users unique_username; Type: CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT unique_username UNIQUE (username);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: workouts workouts_pkey; Type: CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.workouts
    ADD CONSTRAINT workouts_pkey PRIMARY KEY (id);


--
-- Name: idx_exercises_user_id; Type: INDEX; Schema: public; Owner: workout_tracker
--

CREATE INDEX idx_exercises_user_id ON public.exercises USING btree (user_id);


--
-- Name: idx_metrics_type_date; Type: INDEX; Schema: public; Owner: workout_tracker
--

CREATE INDEX idx_metrics_type_date ON public.metrics USING btree (metric_type_id, date);


--
-- Name: idx_programs_user_id; Type: INDEX; Schema: public; Owner: workout_tracker
--

CREATE INDEX idx_programs_user_id ON public.programs USING btree (user_id);


--
-- Name: idx_workouts_user_id; Type: INDEX; Schema: public; Owner: workout_tracker
--

CREATE INDEX idx_workouts_user_id ON public.workouts USING btree (user_id);


--
-- Name: exercises fk_exercises_user_id; Type: FK CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.exercises
    ADD CONSTRAINT fk_exercises_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: metric_types fk_metric_types_user_id; Type: FK CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.metric_types
    ADD CONSTRAINT fk_metric_types_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: metrics fk_metrics_metric_type_id; Type: FK CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.metrics
    ADD CONSTRAINT fk_metrics_metric_type_id FOREIGN KEY (metric_type_id) REFERENCES public.metric_types(id);


--
-- Name: workouts fk_programs_program_id; Type: FK CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.workouts
    ADD CONSTRAINT fk_programs_program_id FOREIGN KEY (program_id) REFERENCES public.programs(id);


--
-- Name: programs fk_programs_user_id; Type: FK CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.programs
    ADD CONSTRAINT fk_programs_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: refresh_token_data fk_refresh_token_data_user_id; Type: FK CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.refresh_token_data
    ADD CONSTRAINT fk_refresh_token_data_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: workouts fk_workouts_user_id; Type: FK CONSTRAINT; Schema: public; Owner: workout_tracker
--

ALTER TABLE ONLY public.workouts
    ADD CONSTRAINT fk_workouts_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--