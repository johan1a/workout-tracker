package se.johan1a.workout.tracker.exception

case class UnauthorizedException(message: String) extends Exception(message)
