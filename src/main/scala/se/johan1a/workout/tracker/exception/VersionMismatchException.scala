package se.johan1a.workout.tracker.exception

case class VersionMismatchException(message: String) extends Exception(message)
