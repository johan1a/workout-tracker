package se.johan1a.workout.tracker.exception

case class InvalidRequest(message: String) extends Exception(message)
