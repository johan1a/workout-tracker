package se.johan1a.workout.tracker.exception

case class NotFoundException(message: String) extends Exception(message)
