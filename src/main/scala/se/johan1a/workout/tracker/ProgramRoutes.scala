package se.johan1a.workout.tracker

import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import se.johan1a.workout.tracker.api.SessionManagement
import se.johan1a.workout.tracker.api.dto.UpsertProgramCommand
import se.johan1a.workout.tracker.auth.UserSession
import se.johan1a.workout.tracker.model.{Program, ProgramListResponse, UserId}
import se.johan1a.workout.tracker.service.ProgramService
import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import se.johan1a.workout.tracker.api.JsonFormat._

import scala.concurrent.{ExecutionContext, Future}

class ProgramRoutes(
    programService: ProgramService,
    sessionManagement: SessionManagement
)(implicit executionContext: ExecutionContext) {

  val routes: Route =
    concat(
      get {
        pathPrefix("programs" / JavaUUID) { id =>
          sessionManagement.withRequiredSession { session: UserSession =>
            val maybeProgram: Future[Option[Program]] =
              programService.fetch(id)

            onSuccess(maybeProgram) {
              case Some(program) if program.userId == session.userId =>
                complete(program)
              case Some(program) => complete(StatusCodes.Forbidden)
              case None          => complete(StatusCodes.NotFound)
            }
          }
        }
      },
      get {
        pathPrefix("programs") {
          sessionManagement.withRequiredSession { session: UserSession =>
            val maybeResult = programService.list(session.userId)

            onSuccess(maybeResult) { programs: Seq[Program] =>
              val response: ProgramListResponse =
                ProgramListResponse(programs)
              complete(response)
            }
          }
        }
      },
      post {
        path("programs") {
          sessionManagement.withRequiredSession { session: UserSession =>
            entity(as[UpsertProgramCommand]) { program =>
              val future: Future[Option[Program]] =
                programService.save(program, UserId(session.userId)).map(p => Some(p))
              onSuccess(future) {
                case Some(program) => complete(program)
                case None          => complete(StatusCodes.Forbidden)
              }
            }
          }
        }
      },
      delete {
        pathPrefix("programs" / JavaUUID) { id =>
          sessionManagement.withRequiredSession { session: UserSession =>
            val future = programService
              .fetch(id)
              .flatMap((programOpt: Option[Program]) => {
                programOpt
                  .map(program => {
                    if (program.userId == session.userId) {
                      programService.delete(id).map {
                        case 0  => StatusCodes.NotFound
                        case id => StatusCodes.OK
                      }
                    } else {
                      Future.successful(StatusCodes.Forbidden)
                    }
                  })
                  .getOrElse(
                    Future.successful(StatusCodes.NotFound)
                  )
              })

            onSuccess(future) { case statusCode =>
              complete(statusCode)
            }
          }
        }
      }
    )

}
