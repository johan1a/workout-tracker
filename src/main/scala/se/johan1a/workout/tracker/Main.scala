package se.johan1a.workout.tracker

import com.typesafe.scalalogging.Logger

import scala.concurrent.Await
import scala.concurrent.duration._

object Main {

  private implicit val logger: Logger = Logger("workout-tracker")

  private val appContext = new AppContext()

  def main(args: Array[String]): Unit = {
    try {
      appContext.initialize()

      val bindingFuture = appContext.routes.get.bind()

      logger.debug("This is debug level")
      logger.info(
        s"Server online at http://localhost:8080/\n"
      )

      Await.result(bindingFuture, Duration.Inf)
    } catch {
      case exception: Exception =>
        logger.error("Shutting down due to Exception", exception)
        appContext.shutdown()
    }
  }
}
