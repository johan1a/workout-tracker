package se.johan1a.workout.tracker.service

import se.johan1a.workout.tracker.db.Exercises
import se.johan1a.workout.tracker.model.Exercise
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend._
import slick.jdbc.JdbcProfile

import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}

class ExerciseService(
    db: Database,
    databaseConfig: DatabaseConfig[JdbcProfile]
)(implicit ec: ExecutionContext) {

  import databaseConfig.profile.api._

  val exercises = TableQuery[Exercises]

  def list(userId: UUID): Future[Seq[Exercise]] =
    db.run(exercises.filter(_.userId === userId).result)

  def fetch(id: UUID): Future[Option[Exercise]] =
    db.run(exercises.filter(_.id === id).result.headOption)

  def save(exercise: Exercise): Future[Exercise] = {
    exercise.id match {
      case Some(value) =>
        for {
          _ <- db
            .run((exercises returning exercises).insertOrUpdate(exercise))
            .map((result: Option[Exercise]) => result.getOrElse(exercise))
        } yield exercise
      case None =>
        insert(exercise)
    }
  }

  def insert(exercise: Exercise): Future[Exercise] = {
    val insertQuery =
      exercises returning exercises.map(_.id) into ((exercise, id) => exercise.copy(id = id))
    val action = insertQuery += exercise
    db.run(action)
  }

}
