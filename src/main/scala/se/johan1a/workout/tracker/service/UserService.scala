package se.johan1a.workout.tracker.service

import com.github.t3hnar.bcrypt._
import com.typesafe.config.Config
import com.typesafe.scalalogging.Logger
import se.johan1a.workout.tracker.db.Users
import se.johan1a.workout.tracker.model.User
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile
import slick.lifted

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Random, Success}

class UserService(
    db: Database,
    databaseConfig: DatabaseConfig[JdbcProfile]
)(implicit
    ec: ExecutionContext,
    config: Config,
    logger: Logger
) {

  import databaseConfig.profile.api._

  private val users = lifted.TableQuery[Users]

  def create(username: String, password: String): Future[User] = {
    password.bcryptSafeBounded match {
      case Success(hashedPassword) =>
        val user = User(None, username, hashedPassword)
        val action = (users returning users) += user
        db.run(action)
      case Failure(exception) =>
        throw exception
    }
  }

  def findByUsername(username: String): Future[Option[User]] = {
    db.run(users.filter(_.username === username).result.headOption)
  }

  def authenticate(
      username: String,
      password: String
  ): Future[Option[User]] = {
    logger.info(s"Authenticating user: $username")
    findByUsername(username).flatMap((userOpt: Option[User]) => {
      userOpt match {
        case Some(user) =>
          password.isBcryptedSafeBounded(user.hashedPassword) match {
            case Success(_) =>
              findByUsername(username)
            case Failure(_) =>
              Future.successful(None)
          }
        case None =>
          Future.successful(None)
      }
    })
  }

  def createDefaultUser(): Future[User] = {
    val username = config.getString("defaultUser.username")

    findByUsername(username).flatMap {
      case Some(user) => Future.successful(user)
      case None =>
        val defaultPassword = config.getString("defaultUser.password")
        val password = if (defaultPassword.nonEmpty) {
          defaultPassword
        } else {
          randomAlphanumericString(64)
        }
        logger.info(s"Creating user $username with password $password")
        create(username, password)
    }
  }

  private def randomAlphanumericString(length: Int): String = {
    def nextAlphaNum: Char = {
      val chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
      chars.charAt(Random.nextInt(chars.length))
    }

    0.until(length).map(_ => nextAlphaNum).mkString
  }

}
