package se.johan1a.workout.tracker.service

import se.johan1a.workout.tracker.api.dto.UpsertProgramCommand
import se.johan1a.workout.tracker.db.Programs
import se.johan1a.workout.tracker.model.{Program, UserId}
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend._
import slick.jdbc.JdbcProfile

import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}

class ProgramService(db: Database, databaseConfig: DatabaseConfig[JdbcProfile])(implicit
    ec: ExecutionContext
) {

  import databaseConfig.profile.api._

  val programs = TableQuery[Programs]

  def list(userId: UUID): Future[Seq[Program]] =
    db.run(programs.filter(_.userId === userId).result)
      .map(programs => programs.map(addExerciseIndicesIfMissing))

  def fetch(id: UUID): Future[Option[Program]] =
    db.run(programs.filter(_.id === id).result.headOption)
      .map(opt => opt.map(addExerciseIndicesIfMissing))

  def save(program: Program): Future[Program] = {
    program.id match {
      case Some(id) =>
        db.run((programs returning programs).insertOrUpdate(program))
          .map((result: Option[Program]) => result.getOrElse(program))
      case None => insert(program)
    }
  }

  def save(command: UpsertProgramCommand, userId: UserId): Future[Program] = {
    val program = UpsertProgramCommand.fromCommand(command, userId)
    save(program)
  }

  def delete(id: UUID): Future[Int] = {
    db.run(programs.filter(_.id === id).delete)
  }

  private def insert(program: Program): Future[Program] = {
    val insertQuery =
      programs returning programs.map(_.id) into ((program, id) => program.copy(id = id))
    val action = insertQuery += program
    db.run(action)
  }

  private def addExerciseIndicesIfMissing(program: Program): Program = {
    program.copy(
      data = program.data.copy(
        weeks = program.data.weeks.map(week =>
          week.copy(
            days = week.days.map(day =>
              day.copy(
                exercises = day.exercises.zipWithIndex.map { case (exercise, i) =>
                  exercise.copy(
                    index = Some(exercise.index.getOrElse(i))
                  )
                }
              )
            )
          )
        )
      )
    )
  }

}
