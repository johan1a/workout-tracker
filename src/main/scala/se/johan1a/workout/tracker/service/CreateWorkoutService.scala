package se.johan1a.workout.tracker.service

import se.johan1a.workout.tracker.model.Workout
import se.johan1a.workout.tracker.model.request.CreateWorkoutRequest

import java.util.UUID
import scala.concurrent.Future

trait CreateWorkoutService {

  def createWorkout(
      userId: UUID,
      createWorkoutRequest: CreateWorkoutRequest
  ): Future[Workout]

}
