package se.johan1a.workout.tracker.service

import se.johan1a.workout.tracker.db.MetricTypes
import se.johan1a.workout.tracker.exception.UnauthorizedException
import se.johan1a.workout.tracker.model.{MetricType, MetricTypeId, UserId}
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

class MetricTypeService(
    db: Database,
    databaseConfig: DatabaseConfig[JdbcProfile]
)(implicit executionContext: ExecutionContext) {

  import databaseConfig.profile.api._

  private val metricTypes = TableQuery[MetricTypes]

  def list(id: UserId): Future[Seq[MetricType]] = {
    db.run(metricTypes.filter(_.userId === id.value).result)
  }

  def fetch(
      userId: UserId,
      metricTypeId: MetricTypeId
  ): Future[Option[MetricType]] = {
    db.run(
      metricTypes
        .filter(row => row.id === metricTypeId.value && row.userId === userId.value)
        .result
        .headOption
    )
  }

  def upsert(userId: UserId, metricType: MetricType): Future[MetricType] = {
    if (userId == metricType.userId) {
      metricType.id match {
        case Some(_) =>
          db.run(metricTypes.insertOrUpdate(metricType)).map(_ => metricType)
        case None =>
          insert(metricType)
      }
    } else {
      Future.failed(throw UnauthorizedException(""))
    }
  }

  def delete(userId: UserId, metricTypeId: MetricTypeId): Future[Unit] = {
    db.run(
      metricTypes
        .filter(row => row.id === metricTypeId.value && row.userId === userId.value)
        .delete
    ).map(_ => ())
  }

  private def insert(metricTye: MetricType): Future[MetricType] = {
    val insertQuery =
      metricTypes returning metricTypes.map(_.id) into ((metricType, id) => {
        metricType.copy(id = id.map(MetricTypeId))
      })
    val action = insertQuery += metricTye
    db.run(action)
  }

}
