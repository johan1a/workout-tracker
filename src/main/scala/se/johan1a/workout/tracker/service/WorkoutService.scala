package se.johan1a.workout.tracker.service

import se.johan1a.workout.tracker.db.Workouts
import se.johan1a.workout.tracker.exception.{NotFoundException, VersionMismatchException}
import se.johan1a.workout.tracker.model.Workout
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend._
import slick.jdbc.JdbcProfile

import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}

class WorkoutService(
    recordsService: RecordsService,
    db: Database,
    databaseConfig: DatabaseConfig[JdbcProfile]
)(implicit
    ec: ExecutionContext
) {

  import databaseConfig.profile.api._

  val workouts = TableQuery[Workouts]

  def list(
      userId: UUID,
      sizeOpt: Option[Int] = None,
      offset: Int = 0
  ): Future[Seq[Workout]] = {
    val query = sizeOpt
      .map { size =>
        workouts
          .filter(_.userId === userId)
          .sortBy(_.date.desc)
          .drop(offset)
          .take(size)
          .result
      }
      .getOrElse(
        workouts
          .filter(_.userId === userId)
          .sortBy(_.date.desc)
          .drop(offset)
          .result
      )
    db.run(query)
  }

  def fetch(id: UUID): Future[Option[Workout]] =
    db.run(workouts.filter(_.id === id).result.headOption)

  def save(userId: UUID, workout: Workout): Future[Workout] = {
    workout.id match {
      case Some(id) =>
        fetch(id).flatMap {
          case Some(existingWorkout) if workout.version == existingWorkout.version + 1 =>
            for {
              _ <- recordsService.saveNewRecords(userId, workout)
              _ <- db
                .run((workouts returning workouts).insertOrUpdate(workout))
                .map((result: Option[Workout]) => result.getOrElse(workout))
            } yield workout
          case Some(existingWorkout) =>
            throw VersionMismatchException(
              s"Version mismatch, expected ${existingWorkout.version + 1} but got ${workout.version}"
            )
          case None => throw NotFoundException(s"workout $id not found")
        }
      case None =>
        for {
          _ <- recordsService.saveNewRecords(userId, workout)
          workout <- insert(workout)
        } yield workout
    }
  }

  def delete(id: UUID): Future[Int] = {
    db.run(workouts.filter(_.id === id).delete)
  }

  private def insert(workout: Workout): Future[Workout] = {
    val insertQuery =
      workouts returning workouts.map(_.id) into ((workout, id) => workout.copy(id = id))
    val action = insertQuery += workout
    db.run(action)
  }

}
