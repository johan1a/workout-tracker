package se.johan1a.workout.tracker.service

import org.apache.pekko.Done
import se.johan1a.workout.tracker.db.Records
import se.johan1a.workout.tracker.model._
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}

class RecordsService(db: Database, databaseConfig: DatabaseConfig[JdbcProfile])(implicit
    ec: ExecutionContext
) {

  import databaseConfig.profile.api._

  val records = TableQuery[Records]

  def fetch(userId: UUID): Future[RecordData] = {
    db.run(records.filter(_.userId === userId).result.headOption)
      .map(userRecords => {
        userRecords.getOrElse(
          RecordData(
            userId,
            Seq.empty
          )
        )
      })
  }

  def saveNewRecords(userId: UUID, workout: Workout): Future[RecordData] = {
    for {
      records <- fetch(userId)
      updatedRecords = updateRecords(workout, records)
      saved <- save(updatedRecords)
    } yield saved
  }

  private def save(recordData: RecordData): Future[RecordData] = {
    db.run((records returning records).insertOrUpdate(recordData))
      .map((result: Option[RecordData]) => result.getOrElse(recordData))
  }

  private def updateRecords(
      workout: Workout,
      records: RecordData
  ): RecordData = {
    var updatedRecordData: RecordData = records
    workout.data.exercises.map { exercise: WorkoutExercise =>
      val exerciseRecordData: ExerciseRecordData =
        updatedRecordData.exerciseRecords
          .find(_.exerciseId == exercise.exerciseId)
          .map(exercise => {
            exercise
              .copy(repMaxes = exercise.repMaxes.filter(_.date != workout.date))
          })
          .getOrElse(ExerciseRecordData(exercise.exerciseId, Seq.empty))

      val newRecordSets = findNewRecordSets(exercise.sets, exerciseRecordData)

      val updatedExerciseRecordData = exerciseRecordData.copy(repMaxes =
        exerciseRecordData.repMaxes.filter(r =>
          !newRecordSets.exists(_.reps.get == r.nbrReps)
        ) ++ newRecordSets.map(set => {
          RepMax(
            nbrReps = set.reps.get,
            weight = set.weight.get,
            date = workout.date,
            index = set.index
          )
        })
      )

      updatedRecordData = updatedRecordData.copy(exerciseRecords =
        updatedExerciseRecordData +: updatedRecordData.exerciseRecords.filter(
          _.exerciseId != exercise.exerciseId
        )
      )
      exercise
    }

    updatedRecordData
  }

  private def findNewRecordSets(
      sets: Seq[ExerciseSet],
      exerciseRecordData: ExerciseRecordData
  ): Set[ExerciseSet] = {
    sets
      .foldLeft(Set[ExerciseSet]())(
        (
            pending: Set[ExerciseSet],
            set: ExerciseSet
        ) =>
          if (
            set.reps.isEmpty || set.checkedAt.isEmpty || set.weight
              .exists(
                _ == 0
              )
          ) {
            pending
          } else {

            val pendingBetter = pending
              .find(s => s.reps == set.reps && s.weight.exists(w => w >= set.weight.get))
              .nonEmpty

            if (pendingBetter) {
              pending
            } else {
              exerciseRecordData.repMaxes.find(repMax =>
                set.reps.contains(repMax.nbrReps) && set.weight.exists(
                  _ <= repMax.weight
                )
              ) match {
                case Some(currentRecord) =>
                  pending
                case None =>
                  pending.filter(s => s.reps != set.reps) + set
              }
            }
          }
      )
  }

  def deleteRecords(userId: UUID): Future[Done] = {
    db.run(records.filter(_.userId === userId).delete).map(_ => Done)
  }

}
