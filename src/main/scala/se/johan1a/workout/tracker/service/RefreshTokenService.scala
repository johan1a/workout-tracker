package se.johan1a.workout.tracker.db

import com.softwaremill.pekkohttpsession.RefreshTokenStorage
import se.johan1a.workout.tracker.auth.UserSession
import scala.concurrent.Future
import com.softwaremill.pekkohttpsession.RefreshTokenLookupResult
import com.softwaremill.pekkohttpsession
import slick.jdbc.PostgresProfile.api._
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.Duration
import se.johan1a.workout.tracker.model.RefreshTokenData
import java.sql.Timestamp
import java.time.Instant
import com.typesafe.scalalogging.Logger

class RefreshTokenService(db: Database)(implicit ec: ExecutionContext, logger: Logger)
    extends RefreshTokenStorage[UserSession] {

  val refreshTokenDataTable = TableQuery[RefreshTokenDataTable]

  override def lookup(selector: String): Future[Option[RefreshTokenLookupResult[UserSession]]] = {
    db.run(refreshTokenDataTable.filter(_.selector === selector).result.headOption).map { dataOpt =>
      dataOpt.map(data =>
        RefreshTokenLookupResult(
          data.tokenHash,
          data.expires.toInstant().toEpochMilli(),
          () => UserSession(data.userId, data.username)
        )
      )
    }
  }

  override def store(
      refreshTokenData: pekkohttpsession.RefreshTokenData[UserSession]
  ): Future[Unit] = {
    val data = RefreshTokenData(
      selector = refreshTokenData.selector,
      userId = refreshTokenData.forSession.userId,
      username = refreshTokenData.forSession.username,
      tokenHash = refreshTokenData.tokenHash,
      expires = Timestamp.from(Instant.ofEpochMilli(refreshTokenData.expires))
    )

    db.run((refreshTokenDataTable += data)).map(_ => ())
  }

  override def remove(selector: String): Future[Unit] =
    db.run(refreshTokenDataTable.filter(_.selector === selector).delete).map(_ => ())

  override def schedule[S](after: Duration)(op: => Future[S]) = {
    logger.info("Running scheduled operation immediately")
    op
    Future.successful(())
  }
}
