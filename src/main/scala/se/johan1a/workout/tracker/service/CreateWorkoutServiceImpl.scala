package se.johan1a.workout.tracker.service

import se.johan1a.workout.tracker.exception.{InvalidRequest, NotFoundException}
import se.johan1a.workout.tracker.model.request.{
  CopyWorkout,
  CreateWorkoutRequest,
  FromProgram,
  New
}
import se.johan1a.workout.tracker.model._

import java.time.LocalDate
import java.util.UUID
import scala.concurrent.{ExecutionContext, Future}

class CreateWorkoutServiceImpl(
    programService: ProgramService,
    workoutService: WorkoutService
)(implicit executionContext: ExecutionContext)
    extends CreateWorkoutService {

  def createWorkout(
      userId: UUID,
      request: CreateWorkoutRequest
  ): Future[Workout] = {
    request.method match {
      case New         => createNewWorkout(userId, request)
      case CopyWorkout => copyWorkout(userId, request)
      case FromProgram => createWorkoutFromProgram(userId, request)
    }
  }

  private def createNewWorkout(
      userId: UUID,
      request: CreateWorkoutRequest
  ): Future[Workout] = {
    workoutService.save(
      userId,
      Workout(
        id = None,
        date = request.date,
        data = WorkoutData(exercises = Seq.empty),
        userId = userId,
        programId = None,
        version = 0
      )
    )
  }

  private def copyWorkout(
      userId: UUID,
      request: CreateWorkoutRequest
  ): Future[Workout] = {
    request.workoutId match {
      case Some(workoutId) =>
        for {
          workoutOpt <- workoutService.fetch(workoutId)
          savedWorkout <- workoutOpt match {
            case Some(workout) =>
              val copy = workout.copy(id = None, date = request.date)
              workoutService.save(userId, copy)
            case None =>
              Future.failed(
                NotFoundException(s"workout with id $workoutId not found")
              )
          }
        } yield savedWorkout
      case None =>
        Future.failed(InvalidRequest("Request missing workoutId"))
    }
  }

  private def createWorkoutFromProgram(
      userId: UUID,
      request: CreateWorkoutRequest
  ): Future[Workout] = {
    request.programId match {
      case Some(programId) =>
        for {
          programOpt <- programService.fetch(programId)
          savedWorkout <- programOpt match {
            case Some(program) =>
              createWorkoutFromProgram(userId, request.date, program)
                .flatMap(workout => {
                  workoutService
                    .save(userId, workout)
                    .flatMap(workout => {
                      programService
                        .save(onWorkoutCreated(program, workout.id.get))
                        .map(_ => workout)
                    })
                })
            case None =>
              Future.failed(
                NotFoundException(s"program with id $programId not found")
              )
          }
        } yield savedWorkout
      case None =>
        Future.failed(InvalidRequest("Request missing workoutId"))
    }
  }

  private def createWorkoutFromProgram(
      userId: UUID,
      date: LocalDate,
      program: Program
  ): Future[Workout] = {
    val nextDay: ProgramDay = getNextProgramDay(program)

    val exercises: Seq[WorkoutExercise] = nextDay.exercises.zipWithIndex.map {
      case (exercise, exerciseIndex) =>
        val sets = exercise.sets.zipWithIndex.map { case (set, setIndex) =>
          ExerciseSet(
            index = setIndex,
            durationSeconds = None,
            checkedAt = None,
            reps = Some(set.nbrReps),
            weight = getSetWeight(program, exercise, set),
            recordMarkers = None
          )
        }

        WorkoutExercise(
          index = exerciseIndex,
          exerciseId = exercise.exerciseId,
          sets = sets
        )
    }

    for {
      supplementaryExercises <- copyPreviousSupplementaryExercises(
        program,
        exercises
      )
    } yield {
      Workout(
        id = None,
        userId = userId,
        date = date,
        data = WorkoutData(
          exercises = exercises ++ supplementaryExercises
        ),
        version = 0,
        programId = program.id
      )
    }
  }

  private def getNextProgramDay(program: Program): ProgramDay = {
    val data = program.data
    val day = data.currentDay
    val week = data.currentWeek
    val daysOfCurrentWeek = data.weeks(week).days

    // This Avoids repeating the same day two times in a row in some programs.
    // e.g. week 3: A - B - A, week 1: B - A - B
    val adjustedDay =
      if (
        data.currentRound % 2 == 1 &&
        data.offsetMode == OffsetMode.ONE
      ) {
        if ((day + 1) != daysOfCurrentWeek.length) {
          day + 1
        } else {
          day - 1
        }
      } else {
        day
      }

    daysOfCurrentWeek(adjustedDay)
  }

  private def getSetWeight(
      program: Program,
      exercise: ProgramExercise,
      set: ProgramSet
  ): Option[Double] = {
    set.fixedWeight match {
      case Some(fixedWeight) =>
        Some(fixedWeight)
      case None =>
        val trainingMaxOpt =
          program.data.trainingMaxes.find(m => m.exerciseId == exercise.exerciseId)
        trainingMaxOpt match {
          case Some(trainingMax) =>
            val max = trainingMax.weights.last
            Some(round(max * (set.percentageOfMax.get / 100)))
          case None =>
            None
        }
    }
  }

  private def copyPreviousSupplementaryExercises(
      program: Program,
      exercises: Seq[WorkoutExercise]
  ): Future[Seq[WorkoutExercise]] = {
    if (program.programType == Program.CUSTOM) {
      Future.successful(Seq.empty)
    } else {
      program.data.createdWorkoutIds.lastOption match {
        case Some(previousWorkoutId) =>
          workoutService.fetch(previousWorkoutId).map {
            case Some(previousWorkout) =>
              // Only copy supplementary work, i.e. no main lifts (that have TMs).
              val exerciseIdsToIgnore =
                program.data.trainingMaxes.map(m => m.exerciseId)

              var nbrAdded = 0
              previousWorkout.data.exercises
                .map { prevExercise =>
                  if (!exerciseIdsToIgnore.contains(prevExercise.exerciseId)) {
                    val exercise = Some(
                      prevExercise.copy(
                        index = exercises.length + nbrAdded,
                        sets = prevExercise.sets.map(set => set.copy(checkedAt = None))
                      )
                    )
                    nbrAdded += 1
                    exercise
                  } else None
                }
                .collect { case Some(exercise) => exercise }
            case None => Seq.empty
          }
        case None => Future.successful(Seq.empty)
      }
    }
  }

  private def onWorkoutCreated(program: Program, workoutId: UUID): Program = {
    val data = program.data

    var day = data.currentDay + 1
    var week = data.currentWeek
    var round = data.currentRound
    var trainingMaxes = data.trainingMaxes
    if (data.currentDay + 1 == data.weeks(data.currentWeek).days.size) {
      day = 0
      week = data.currentWeek + 1
    }
    if (week == data.weeks.size) {
      week = 0
      day = 0
      round = round + 1
      trainingMaxes = updateTrainingMaxes(trainingMaxes)
    }

    val updated = program.copy(data =
      data.copy(
        currentDay = day,
        currentWeek = week,
        currentRound = round,
        trainingMaxes = trainingMaxes,
        createdWorkoutIds = data.createdWorkoutIds :+ workoutId
      )
    )

    updated
  }

  private def updateTrainingMaxes(
      trainingMaxes: Seq[TrainingMax]
  ): Seq[TrainingMax] = {
    trainingMaxes.map(max =>
      max.copy(weights = max.weights :+ (max.weights.last + max.defaultIncrement))
    )
  }

  private def round(n: Double): Double = {
    Math.ceil(n / 2.5) * 2.5
  }

}
