package se.johan1a.workout.tracker.service

import se.johan1a.workout.tracker.db.Metrics
import se.johan1a.workout.tracker.exception.{NotFoundException, UnauthorizedException}
import se.johan1a.workout.tracker.model.{Metric, MetricType, MetricTypeId, UserId}
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import java.time.LocalDate
import scala.concurrent.{ExecutionContext, Future}

class MetricService(
    metricTypeService: MetricTypeService,
    db: Database,
    databaseConfig: DatabaseConfig[JdbcProfile]
)(implicit executionContext: ExecutionContext) {

  import databaseConfig.profile.api._

  private val metrics = TableQuery[Metrics]

  def list(userId: UserId, metricTypeId: MetricTypeId): Future[Seq[Metric]] = {
    for {
      metricType: Option[MetricType] <- metricTypeService.fetch(
        userId,
        metricTypeId
      )
      metrics: Seq[Metric] <- db.run(
        metrics.filter(_.metricTypeId === metricTypeId.value).sortBy(_.date.asc).result
      )
    } yield {
      metricType match {
        case Some(value) if value.userId == userId => metrics
        case _ => throw NotFoundException(s"MetricType $metricTypeId not found")
      }
    }
  }

  def fetch(
      userId: UserId,
      metricTypeId: MetricTypeId,
      date: LocalDate
  ): Future[Option[Metric]] = {
    for {
      metricTypeOpt: Option[MetricType] <- metricTypeService.fetch(
        userId,
        metricTypeId
      )
      metric <- metricTypeOpt match {
        case Some(metricType) if metricType.userId == userId =>
          db.run(
            metrics
              .filter(row => row.metricTypeId === metricTypeId.value && row.date === date)
              .result
              .headOption
          )
        case Some(_) =>
          Future.failed(
            UnauthorizedException(
              s"User $userId does not have access to MetricType $metricTypeId"
            )
          )
        case None =>
          Future.failed(
            NotFoundException(
              s"MetricType $metricTypeId not found"
            )
          )
      }
    } yield metric
  }

  def upsert(userId: UserId, metric: Metric): Future[Metric] = {
    for {
      metricType <- metricTypeService.fetch(userId, metric.metricTypeId)
      existing <- fetch(userId, metric.metricTypeId, metric.date)
      _ <- existing match {
        case Some(_) if metricType.map(_.userId).contains(userId) =>
          db.run(
            metrics
              .filter(row =>
                row.metricTypeId === metric.metricTypeId.value && row.date === metric.date
              )
              .update(metric)
          ).map(_ => metric)
        case None if metricType.map(_.userId).contains(userId) =>
          insert(metric)
        case _ =>
          Future.failed(
            UnauthorizedException(
              s"User $userId does not have access to MetricType ${metric.metricTypeId}"
            )
          )
      }
    } yield metric
  }

  def delete(
      userId: UserId,
      metricTypeId: MetricTypeId,
      date: LocalDate
  ): Future[Unit] = {
    for {
      metricTypeOpt <- metricTypeService.fetch(userId, metricTypeId)
      _ <- metricTypeOpt match {
        case Some(metricType) if metricType.id.contains(metricTypeId) =>
          db.run(
            metrics
              .filter(row => row.metricTypeId === metricTypeId.value && row.date === date)
              .delete
          ).map(_ => ())
        case None =>
          throw NotFoundException(s"MetricType $metricTypeId not found")
        case _ =>
          throw UnauthorizedException(
            s"User $userId does not have access to MetricType $metricTypeId"
          )
      }
    } yield {}
  }

  private def insert(metricType: Metric): Future[Metric] = {
    val insertQuery =
      metrics returning metrics.map(_.metricTypeId) into ((metric, id) =>
        metric.copy(metricTypeId = MetricTypeId(id))
      )
    val action = insertQuery += metricType
    db.run(action)
  }

}
