package se.johan1a.workout.tracker.db

import se.johan1a.workout.tracker.api.JsonFormat._
import se.johan1a.workout.tracker.model.{ExerciseRecordData, RecordData}
import slick.jdbc.PostgresProfile.api._
import spray.json.DefaultJsonProtocol.immSeqFormat
import spray.json._

import java.util.UUID

object Records {
  type Row = (UUID, String)
}

class Records(tag: Tag) extends Table[RecordData](tag, "records") {

  def userId =
    column[UUID](
      "user_id",
      O.PrimaryKey,
      O.SqlType("UUID")
    )

  def exerciseRecords = column[String]("exercise_records")

  def * = (userId, exerciseRecords) <> (tupled, unapply)

  import Records.Row

  def unapply(records: RecordData): Option[Row] = {
    Some(
      (
        records.userId,
        records.exerciseRecords.toJson.compactPrint
      )
    )
  }

  def tupled(row: Row): RecordData = {
    val exerciseRecords = row._2.parseJson.convertTo[Seq[ExerciseRecordData]]
    RecordData(row._1, exerciseRecords)
  }
}
