package se.johan1a.workout.tracker.db

import se.johan1a.workout.tracker.api.JsonFormat._
import se.johan1a.workout.tracker.model._
import slick.jdbc.PostgresProfile.api._
import spray.json._

import java.util.UUID

object Programs {
  type Row = (Option[UUID], Int, String, String, UUID)
}

class Programs(tag: Tag) extends Table[Program](tag, "programs") {
  def id =
    column[Option[UUID]](
      "id",
      O.PrimaryKey,
      O.AutoInc,
      O.SqlType("UUID")
    )
  def programType = column[Int]("program_type")
  def name = column[String]("name")
  def data = column[String]("data")
  def userId = column[UUID]("user_id", O.SqlType("UUID"))

  def * = (id, programType, name, data, userId) <> (tupled, unapply)

  import Programs.Row

  def unapply(program: Program): Option[Row] = {
    Some(
      (
        program.id,
        program.programType,
        program.name,
        program.data.toJson.compactPrint,
        program.userId
      )
    )
  }

  def tupled(row: Row): Program = {
    val data = row._4.parseJson.convertTo[ProgramData]
    Program(row._1, row._2, row._3, data, row._5)
  }
}
