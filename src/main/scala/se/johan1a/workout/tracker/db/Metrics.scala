package se.johan1a.workout.tracker.db

import se.johan1a.workout.tracker.model.{Metric, MetricTypeId}
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

import java.time.LocalDate
import java.util.UUID

class Metrics(tag: Tag) extends Table[Metric](tag, "metrics") {

  def metricTypeId =
    column[UUID](
      "metric_type_id",
      O.SqlType("UUID")
    )

  def date = column[LocalDate]("date")

  def value = column[Int]("value")

  def note = column[Option[String]]("note")

  def * =
    (metricTypeId, date, value, note) <> (Metrics.tupled, Metrics.unapply)
}

object Metrics {
  type MetricRow = (UUID, LocalDate, Int, Option[String])
  def tupled: MetricRow => Metric = { case (metricTypeId, date, value, note) =>
    Metric(
      MetricTypeId(metricTypeId),
      date,
      value,
      note
    )
  }

  def unapply: Metric => Option[MetricRow] = { case Metric(metricTypeId, date, value, note) =>
    Some(metricTypeId.value, date, value, note)

  }

}
