package se.johan1a.workout.tracker.db

import se.johan1a.workout.tracker.model.User
import slick.jdbc.PostgresProfile.api._

import java.util.UUID

object Users {
  type Row = (Option[UUID], String, String)
}

class Users(tag: Tag) extends Table[User](tag, "users") {
  def id: Rep[Option[UUID]] =
    column[Option[UUID]](
      "id",
      O.PrimaryKey,
      O.AutoInc,
      O.SqlType("UUID")
    )
  def username = column[String]("username")
  def hashedPasssword = column[String]("hashed_password")

  def * = (id, username, hashedPasssword) <> (tupled, unapply)

  import Users.Row

  def unapply(user: User): Option[Row] = {
    Some(
      (
        user.id,
        user.username,
        user.hashedPassword
      )
    )
  }

  def tupled(row: Row): User = {
    User(row._1, row._2, row._3)
  }
}
