package se.johan1a.workout.tracker.db
import se.johan1a.workout.tracker.model.{MetricType, MetricTypeId, MetricTypeName, UserId}
import slick.jdbc.PostgresProfile.api._

import java.util.UUID

class MetricTypes(tag: Tag) extends Table[MetricType](tag, "metric_types") {

  def id =
    column[Option[UUID]](
      "id",
      O.PrimaryKey,
      O.AutoInc,
      O.SqlType("UUID")
    )
  def userId = column[UUID]("user_id", O.SqlType("UUID"))
  def name = column[String]("name")
  def minValue = column[Option[Int]]("min_value")
  def maxValue = column[Option[Int]]("max_value")
  def * =
    (
      id,
      userId,
      name,
      minValue,
      maxValue
    ) <> (MetricTypes.tupled, MetricTypes.unapply)
}

object MetricTypes {
  type MetricRow = (Option[UUID], UUID, String, Option[Int], Option[Int])
  def tupled: MetricRow => MetricType = { case (id, userId, name, minValue, maxValue) =>
    MetricType(
      id.map(MetricTypeId.apply),
      UserId(userId),
      MetricTypeName(name),
      minValue,
      maxValue
    )
  }

  def unapply: MetricType => Option[MetricRow] = {
    case MetricType(id, userId, name, minvalue, maxValue) =>
      Some(id.map(_.value), userId.value, name.value, minvalue, maxValue)

  }

}
