package se.johan1a.workout.tracker.db

import slick.jdbc.PostgresProfile.api._

import se.johan1a.workout.tracker.model.Exercise

import java.util.UUID

class Exercises(tag: Tag) extends Table[Exercise](tag, "exercises") {
  def id =
    column[Option[UUID]](
      "id",
      O.PrimaryKey,
      O.AutoInc,
      O.SqlType("UUID")
    )
  def name = column[String]("name")
  def exerciseType = column[String]("exercise_type")
  def userId = column[UUID]("user_id", O.SqlType("UUID"))
  def * =
    (id, name, exerciseType, userId) <> (Exercise.tupled, Exercise.unapply)
}
