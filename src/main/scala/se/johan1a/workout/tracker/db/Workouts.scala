package se.johan1a.workout.tracker.db

import slick.jdbc.PostgresProfile.api._

import se.johan1a.workout.tracker.model._
import spray.json._
import se.johan1a.workout.tracker.api.JsonFormat._

import java.util.UUID
import java.time.LocalDate

object Workouts {
  type Row = (Option[UUID], LocalDate, String, UUID, Option[UUID], Int)
}

class Workouts(tag: Tag) extends Table[Workout](tag, None, "workouts") {
  def id =
    column[Option[UUID]](
      "id",
      O.PrimaryKey,
      O.AutoInc,
      O.SqlType("UUID")
    )
  def date = column[LocalDate]("date")
  def data = column[String]("data")
  def userId = column[UUID]("user_id", O.SqlType("UUID"))
  def programId = column[Option[UUID]]("program_id", O.SqlType("UUID"))

  def version = column[Int]("version")

  def * = (id, date, data, userId, programId, version) <> (tupled, unapply)

  import Workouts.Row

  def unapply(workout: Workout): Option[Row] = {
    Some(
      (
        workout.id,
        workout.date,
        workout.data.toJson.compactPrint,
        workout.userId,
        workout.programId,
        workout.version
      )
    )
  }

  def tupled(row: Row): Workout = {
    val data = row._3.parseJson.convertTo[WorkoutData]
    Workout(row._1, row._2, data, userId = row._4, programId = row._5, version = row._6)
  }
}
