package se.johan1a.workout.tracker.db

import slick.jdbc.PostgresProfile.api._

import java.util.UUID
import se.johan1a.workout.tracker.model.RefreshTokenData
import java.sql.Timestamp

class RefreshTokenDataTable(tag: Tag) extends Table[RefreshTokenData](tag, "refresh_token_data") {

  def selector = column[String]("selector", O.PrimaryKey)
  def userId = column[UUID]("user_id")
  def username = column[String]("username")
  def tokenHash = column[String]("token_hash")
  def expires = column[Timestamp]("expires")

  def * = (
    selector,
    userId,
    username,
    tokenHash,
    expires
  ) <> (RefreshTokenData.tupled, RefreshTokenData.unapply)
}
