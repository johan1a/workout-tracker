package se.johan1a.workout.tracker.db

import com.typesafe.scalalogging.Logger
import liquibase.Liquibase
import liquibase.database.core.PostgresDatabase
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.{ClassLoaderResourceAccessor, ResourceAccessor}
import slick.jdbc.JdbcBackend

object LiquibaseRunner {

  private implicit val logger: Logger = Logger("LiquibaseRunner")

  def run(db: JdbcBackend.Database): Unit = {
    val resourceAccessor: ResourceAccessor = new ClassLoaderResourceAccessor()
    val connection = new JdbcConnection(db.source.createConnection())
    val database = new PostgresDatabase()
    database.setConnection(connection)
    val liquibase = new Liquibase("liquibase/dbchangelog.yaml", resourceAccessor, database)
    logger.info("Running liquibase update")
    liquibase.update()
  }

}
