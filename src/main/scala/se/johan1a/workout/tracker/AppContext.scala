package se.johan1a.workout.tracker

import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.Logger
import org.apache.pekko.actor.ActorSystem
import se.johan1a.workout.tracker.api.{
  AuthRoutes,
  ExerciseRoutes,
  MetricRoutes,
  RecordRoutes,
  Routes,
  SessionManagement,
  WorkoutRoutes
}
import se.johan1a.workout.tracker.db.{LiquibaseRunner, RefreshTokenService}
import se.johan1a.workout.tracker.service._
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend._
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContextExecutor}

class AppContext(implicit logger: Logger) {

  object Implicits {
    implicit val system: ActorSystem = ActorSystem("workout-tracker")
    implicit val executionContext: ExecutionContextExecutor = system.dispatcher
    implicit val config: Config = ConfigFactory.load
  }

  var routes: Option[Routes] = None

  def initialize(): Unit = {
    import Implicits._

    val databaseConfig: DatabaseConfig[JdbcProfile] =
      DatabaseConfig.forConfig[JdbcProfile]("database")

    val db: Database = Database.forConfig("database")
    implicit val refreshTokenStorage = new RefreshTokenService(db)
    val sessionManagement = new SessionManagement()
    val recordsService = new RecordsService(db, databaseConfig)
    val workoutService = new WorkoutService(recordsService, db, databaseConfig)
    val exerciseService = new ExerciseService(db, databaseConfig)
    val programService = new ProgramService(db, databaseConfig)
    val userService = new UserService(db, databaseConfig)
    val createWorkoutService =
      new CreateWorkoutServiceImpl(programService, workoutService)
    val metricTypeService = new MetricTypeService(db, databaseConfig)
    val metricService = new MetricService(metricTypeService, db, databaseConfig)
    val metricRoutes =
      new MetricRoutes(sessionManagement, metricTypeService, metricService)
    val authRoutes = new AuthRoutes(userService, sessionManagement)
    val workoutRoutes = new WorkoutRoutes(workoutService, createWorkoutService, sessionManagement)
    val programRoutes = new ProgramRoutes(programService, sessionManagement)
    val exerciseRoutes = new ExerciseRoutes(exerciseService, sessionManagement)
    val recordRoutes = new RecordRoutes(recordsService, workoutService, sessionManagement)

    LiquibaseRunner.run(db)

    Await.result(userService.createDefaultUser(), 60.seconds)

    routes = Some(
      new Routes(
        metricRoutes,
        authRoutes,
        workoutRoutes,
        programRoutes,
        exerciseRoutes,
        recordRoutes
      )
    )
  }

  def shutdown(): Unit = {
    Implicits.system.terminate()
  }

}
