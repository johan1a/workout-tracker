package se.johan1a.workout.tracker.auth

import com.softwaremill.pekkohttpsession.{
  JValueSessionSerializer,
  JwtSessionEncoder,
  SessionSerializer
}
import org.json4s.ext.JavaTypesSerializers
import org.json4s.jackson.Serialization
import org.json4s.{Formats, JValue, NoTypeHints}

import java.util.UUID

object UserSession {

  implicit val sessionSerializer: SessionSerializer[UserSession, JValue] =
    JValueSessionSerializer.caseClass[UserSession]

  implicit val sessionEncoder: JwtSessionEncoder[UserSession] =
    new JwtSessionEncoder()

  implicit lazy val formats: Formats =
    Serialization.formats(NoTypeHints) ++ JavaTypesSerializers.all
}

case class UserSession(userId: UUID, username: String)
