package se.johan1a.workout.tracker.model

import java.time.LocalDateTime

case class ExerciseSet(
    index: Int,
    reps: Option[Int],
    weight: Option[Double],
    durationSeconds: Option[Int],
    checkedAt: Option[LocalDateTime],
    recordMarkers: Option[RecordMarker] // Deprecated, do not use
)
