package se.johan1a.workout.tracker.model

import java.time.LocalDate
import java.util.UUID

case class RepMax(nbrReps: Int, weight: Double, date: LocalDate, index: Int)

case class ExerciseRecordData(exerciseId: UUID, repMaxes: Seq[RepMax])

case class RecordData(userId: UUID, exerciseRecords: Seq[ExerciseRecordData])
