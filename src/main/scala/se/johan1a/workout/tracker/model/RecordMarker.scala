package se.johan1a.workout.tracker.model

object RecordTypes {
  val repMax = 0
  val durationMax = 1
}

case class RecordMarker(recordType: Int)
