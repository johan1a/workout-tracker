package se.johan1a.workout.tracker.model

import java.util.UUID

case class WorkoutExercise(
    index: Int,
    exerciseId: UUID,
    sets: Seq[ExerciseSet]
)
