package se.johan1a.workout.tracker.model

import java.util.UUID

case class ProgramExercise(index: Option[Int], exerciseId: UUID, sets: Seq[ProgramSet])
