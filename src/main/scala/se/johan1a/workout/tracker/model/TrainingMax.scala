package se.johan1a.workout.tracker.model

import java.util.UUID

// Last weight is the current training max for the exercise
case class TrainingMax(
    exerciseId: UUID,
    weights: Seq[Double],
    defaultIncrement: Double
)
