package se.johan1a.workout.tracker.model

case class ProgramWeek(index: Int, days: Seq[ProgramDay])
