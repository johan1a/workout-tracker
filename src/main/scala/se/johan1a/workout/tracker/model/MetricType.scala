package se.johan1a.workout.tracker.model

import java.util.UUID

case class MetricTypeId(value: UUID)

case class MetricTypeName(value: String)

case class MetricType(
    id: Option[MetricTypeId],
    userId: UserId,
    name: MetricTypeName,
    // Scaled by 100
    minValue: Option[Int],
    maxValue: Option[Int]
)
