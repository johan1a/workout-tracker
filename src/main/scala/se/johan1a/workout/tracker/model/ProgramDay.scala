package se.johan1a.workout.tracker.model

case class ProgramDay(index: Int, exercises: Seq[ProgramExercise])
