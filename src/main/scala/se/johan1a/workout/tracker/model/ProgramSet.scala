package se.johan1a.workout.tracker.model

case class ProgramSet(
    index: Int,
    percentageOfMax: Option[Double],
    fixedWeight: Option[Double],
    nbrReps: Int
)
