package se.johan1a.workout.tracker.model

case class WorkoutData(exercises: Seq[WorkoutExercise])
