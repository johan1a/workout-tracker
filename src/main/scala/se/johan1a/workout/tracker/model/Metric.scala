package se.johan1a.workout.tracker.model

import java.time.LocalDate

case class Metric(
    metricTypeId: MetricTypeId,
    date: LocalDate,
    // Scaled by 100
    value: Int,
    note: Option[String]
)
