package se.johan1a.workout.tracker.model

import java.time.LocalDate
import java.util.UUID

case class Workout(
    id: Option[UUID],
    date: LocalDate,
    data: WorkoutData,
    programId: Option[UUID],
    userId: UUID,
    version: Int
)
