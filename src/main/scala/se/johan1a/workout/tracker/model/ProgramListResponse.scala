package se.johan1a.workout.tracker.model

case class ProgramListResponse(programs: Seq[Program])
