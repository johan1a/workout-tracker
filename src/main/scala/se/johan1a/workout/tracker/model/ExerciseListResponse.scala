package se.johan1a.workout.tracker.model

case class ExerciseListResponse(exercises: Seq[Exercise])
