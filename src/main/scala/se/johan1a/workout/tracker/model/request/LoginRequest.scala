package se.johan1a.workout.tracker.model.request

case class LoginRequest(username: String, password: String)
