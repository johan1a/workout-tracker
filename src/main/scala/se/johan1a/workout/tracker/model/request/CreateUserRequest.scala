package se.johan1a.workout.tracker.model.request

case class CreateUserRequest(username: String, password: String)
