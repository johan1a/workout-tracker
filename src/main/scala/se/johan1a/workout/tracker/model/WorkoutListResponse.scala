package se.johan1a.workout.tracker.model

import se.johan1a.workout.tracker.model.Workout

case class WorkoutListResponse(workouts: Seq[Workout])
