package se.johan1a.workout.tracker.model

import java.util.UUID

object Program {
  val TYPE_531_FOR_BEGINNERS = 0
  val CUSTOM = 1
}

case class Program(
    id: Option[UUID],
    programType: Int,
    name: String,
    data: ProgramData,
    userId: UUID
)
