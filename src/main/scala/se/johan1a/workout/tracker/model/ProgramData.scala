package se.johan1a.workout.tracker.model

import java.util.UUID

object OffsetMode {
  val ZERO = 0
  // Every other round, offset days by one.
  // To alternate between A - B - A and B - A - B
  val ONE = 1
}

case class ProgramData(
    weeks: Seq[ProgramWeek],
    trainingMaxes: Seq[TrainingMax],
    currentRound: Int,
    currentDay: Int,
    currentWeek: Int,
    offsetMode: Int,
    createdWorkoutIds: Seq[UUID]
)
