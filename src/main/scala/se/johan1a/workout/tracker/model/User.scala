package se.johan1a.workout.tracker.model

import java.util.UUID

case class UserId(value: UUID)

case class User(id: Option[UUID], username: String, hashedPassword: String)
