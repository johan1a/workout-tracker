package se.johan1a.workout.tracker.model

import java.util.UUID
import java.sql.Timestamp

case class RefreshTokenData(
    selector: String,
    userId: UUID,
    username: String,
    tokenHash: String,
    expires: Timestamp
)
