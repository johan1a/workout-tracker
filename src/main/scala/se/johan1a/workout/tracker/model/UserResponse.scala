package se.johan1a.workout.tracker.model

import se.johan1a.workout.tracker.auth.UserSession

import java.util.UUID
object UserResponse {
  def create(session: UserSession) = {
    UserResponse(
      UserResponseUser(id = session.userId, username = session.username)
    )
  }
}

case class UserResponseUser(id: UUID, username: String)
case class UserResponse(user: UserResponseUser)
