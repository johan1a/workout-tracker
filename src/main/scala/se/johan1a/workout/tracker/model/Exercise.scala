package se.johan1a.workout.tracker.model

import java.util.UUID

object ExerciseType {
  val repsAndWeight = "repsAndWeight"
  val reps = "reps"
  val duration = "duration"
  val distance = "distance"
}

case class Exercise(
    id: Option[UUID],
    name: String,
    exerciseType: String,
    userId: UUID
)
