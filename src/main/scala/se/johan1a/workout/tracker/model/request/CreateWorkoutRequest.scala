package se.johan1a.workout.tracker.model.request

import java.time.LocalDate
import java.util.UUID

sealed trait CreateWorkoutMethod
case object FromProgram extends CreateWorkoutMethod
case object New extends CreateWorkoutMethod
case object CopyWorkout extends CreateWorkoutMethod

case class CreateWorkoutRequest(
    date: LocalDate,
    method: CreateWorkoutMethod,
    programId: Option[UUID],
    workoutId: Option[UUID]
)
