package se.johan1a.workout.tracker.api.dto

import java.util.UUID

case class UpsertProgramExercise(index: Option[Int], exerciseId: UUID, sets: Seq[UpsertProgramSet])
