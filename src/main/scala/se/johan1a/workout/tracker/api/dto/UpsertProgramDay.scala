package se.johan1a.workout.tracker.api.dto

case class UpsertProgramDay(index: Option[Int], exercises: Seq[UpsertProgramExercise])
