package se.johan1a.workout.tracker.api

import com.typesafe.scalalogging.Logger
import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.model.headers.{BasicHttpCredentials, HttpChallenges}
import org.apache.pekko.http.scaladsl.server.AuthenticationFailedRejection.{
  CredentialsMissing,
  CredentialsRejected
}
import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.{AuthenticationFailedRejection, Directive1, Route}
import se.johan1a.workout.tracker.api.JsonFormat._
import se.johan1a.workout.tracker.auth.UserSession
import se.johan1a.workout.tracker.model.UserResponse
import se.johan1a.workout.tracker.model.request.CreateUserRequest
import se.johan1a.workout.tracker.service.UserService

import scala.concurrent.Future

class AuthRoutes(
    userService: UserService,
    sessionManagement: SessionManagement
) {

  private val logger = Logger("AuthRoutes")

  val routes: Route =
    pathPrefix("auth") {
      concat(
        post {
          path("login") {
            authenticateBasicAsync(
              realm = "secure site",
              userService.authenticate
            ) { user =>
              logger.info(s"Logged in user: ${user.username}")
              sessionManagement.mySetSession(
                UserSession(user.id.get, user.username)
              ) {
                complete("OK")
              }
            }
          } ~
            path("logout") {
              sessionManagement.withRequiredSession { session: UserSession =>
                logger.info(s"Logging out ${session.username}")
                sessionManagement.myInvalidateSession {
                  complete(StatusCodes.OK)
                }
              }
            }
        },
        path("user") {
          get {
            sessionManagement.withRequiredSession { session: UserSession =>
              complete(UserResponse.create(session))
            }
          }
        },
        path("users") {
          post {
            entity(as[CreateUserRequest]) { request =>
              sessionManagement.withRequiredSession { _: UserSession =>
                val future =
                  userService.create(request.username, request.password)

                onSuccess(future)(_ => complete(StatusCodes.Created))
              }
            }
          }
        }
      )
    }

  // Stolen from https://synkre.com/bcrypt-for-akka-http-password-encryption/
  private def authenticateBasicAsync[T](
      realm: String,
      authenticate: (String, String) => Future[Option[T]]
  ): Directive1[T] = {
    def challenge = HttpChallenges.basic(realm)

    extractCredentials.flatMap {
      case Some(BasicHttpCredentials(username, password)) =>
        onSuccess(authenticate(username, password)).flatMap {
          case Some(client) => provide(client)
          case None =>
            reject(
              AuthenticationFailedRejection(CredentialsRejected, challenge)
            )
        }
      case _ =>
        reject(AuthenticationFailedRejection(CredentialsMissing, challenge))
    }
  }

}
