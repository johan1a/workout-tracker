package se.johan1a.workout.tracker.api.dto

case class UpsertProgramSet(
    index: Option[Int],
    percentageOfMax: Option[Double],
    fixedWeight: Option[Double],
    nbrReps: Int
)
