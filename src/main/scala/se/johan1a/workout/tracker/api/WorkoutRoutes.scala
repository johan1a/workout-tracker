package se.johan1a.workout.tracker.api

import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import se.johan1a.workout.tracker.api.JsonFormat._
import se.johan1a.workout.tracker.auth.UserSession
import se.johan1a.workout.tracker.exception.{NotFoundException, VersionMismatchException}
import se.johan1a.workout.tracker.model.request.CreateWorkoutRequest
import se.johan1a.workout.tracker.model.{Workout, WorkoutListResponse}
import se.johan1a.workout.tracker.service._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class WorkoutRoutes(
    workoutService: WorkoutService,
    createWorkoutService: CreateWorkoutService,
    sessionManagement: SessionManagement
)(implicit executionContext: ExecutionContext) {

  val routes: Route = concat(
    get {
      pathPrefix("workouts" / JavaUUID) { id =>
        sessionManagement.withRequiredSession { session: UserSession =>
          val maybeWorkout: Future[Option[Workout]] =
            workoutService.fetch(id)

          onSuccess(maybeWorkout) {
            case Some(workout) if workout.userId == session.userId =>
              complete(workout)
            case Some(workout) => complete(StatusCodes.Forbidden)
            case None          => complete(StatusCodes.NotFound)
          }
        }
      }
    },
    delete {
      pathPrefix("workouts" / JavaUUID) { id =>
        sessionManagement.withRequiredSession { session: UserSession =>
          val future = workoutService
            .fetch(id)
            .flatMap((workoutOpt: Option[Workout]) => {
              workoutOpt
                .map(workout => {
                  if (workout.userId == session.userId) {
                    workoutService.delete(id).map {
                      case 0  => StatusCodes.NotFound
                      case id => StatusCodes.OK
                    }
                  } else {
                    Future.successful(StatusCodes.Forbidden)
                  }
                })
                .getOrElse(
                  Future.successful(StatusCodes.NotFound)
                )
            })

          onSuccess(future) { case statusCode =>
            complete(statusCode)
          }
        }
      }
    },
    get {
      pathPrefix("workouts") {
        parameters("size".optional, "offset".optional) { (size, offset) =>
          sessionManagement.withRequiredSession { session: UserSession =>
            val maybeResult =
              workoutService.list(
                session.userId,
                size.map(_.toInt),
                offset.map(_.toInt).getOrElse(0)
              )

            onSuccess(maybeResult) { workouts: Seq[Workout] =>
              complete(WorkoutListResponse(workouts))
            }
          }
        }
      }
    },
    post {
      path("workouts") {
        sessionManagement.withRequiredSession { session: UserSession =>
          entity(as[Workout]) { workout =>

            val future: Future[Option[Workout]] =
              if (workout.userId == session.userId) {
                workoutService
                  .save(session.userId, workout)
                  .map(w => Some(w))
              } else {
                Future.successful(None)
              }

            onComplete(future) {
              case Success(Some(saved)) => complete(saved)
              case Success(None)        => complete(StatusCodes.Forbidden)
              case Failure(exception) =>
                exception match {
                  case e: VersionMismatchException =>
                    complete(StatusCodes.BadRequest, e.message)
                  case e: NotFoundException =>
                    complete(StatusCodes.NotFound, e.message)
                  case e => throw e
                }
            }
          }
        }
      } ~ path("workouts" / "create") {
        sessionManagement.withRequiredSession { session: UserSession =>
          entity(as[CreateWorkoutRequest]) { createWorkoutRequest =>
            val future = createWorkoutService.createWorkout(
              session.userId,
              createWorkoutRequest
            )

            onComplete(future) {
              case Success(saved) =>
                complete(StatusCodes.Created, saved)
              case Failure(exception) =>
                exception match {
                  case e: NotFoundException =>
                    complete(StatusCodes.NotFound, e.message)
                  case e => throw e
                }
            }
          }
        }
      }
    }
  )

}
