package se.johan1a.workout.tracker.api

import com.typesafe.scalalogging.Logger
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import se.johan1a.workout.tracker.ProgramRoutes

import scala.concurrent.Future

class Routes(
    metricRoutes: MetricRoutes,
    authRoutes: AuthRoutes,
    workoutRoutes: WorkoutRoutes,
    programRoutes: ProgramRoutes,
    exerciseRoutes: ExerciseRoutes,
    recordRoutes: RecordRoutes
)(implicit
    system: ActorSystem,
    logger: Logger
) {

  def bind(): Future[Http.ServerBinding] = {
    Http()
      .newServerAt("0.0.0.0", 8080)
      .bind(routes)
  }

  val routes: Route =
    concat(
      pathPrefix("api") {
        concat(
          metricRoutes.routes,
          authRoutes.routes,
          workoutRoutes.routes,
          programRoutes.routes,
          exerciseRoutes.routes,
          recordRoutes.routes,
          post {
            pathPrefix("log") {
              entity(as[String]) { message =>
                logger.info(s"Message from client: $message")
                complete("OK")
              }
            }
          }
        )
      }
    )

}
