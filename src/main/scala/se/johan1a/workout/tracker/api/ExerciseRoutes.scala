package se.johan1a.workout.tracker.api

import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import se.johan1a.workout.tracker.api.JsonFormat._
import se.johan1a.workout.tracker.auth.UserSession
import se.johan1a.workout.tracker.model.{Exercise, ExerciseListResponse}
import se.johan1a.workout.tracker.service.ExerciseService

import scala.concurrent.{ExecutionContext, Future}

class ExerciseRoutes(exerciseService: ExerciseService, sessionManagement: SessionManagement)(
    implicit executionContext: ExecutionContext
) {

  val routes: Route =
    concat(
      get {
        pathPrefix("exercises" / JavaUUID) { id =>
          sessionManagement.withRequiredSession { session: UserSession =>
            val maybeExercise: Future[Option[Exercise]] =
              exerciseService.fetch(id)

            onSuccess(maybeExercise) {
              case Some(exercise) if exercise.userId == session.userId =>
                complete(exercise)
              case Some(exercise) => complete(StatusCodes.Forbidden)
              case None           => complete(StatusCodes.NotFound)
            }
          }
        }
      },
      get {
        pathPrefix("exercises") {
          sessionManagement.withRequiredSession { session: UserSession =>
            val maybeResult = exerciseService.list(session.userId)

            onSuccess(maybeResult) { exercises: Seq[Exercise] =>
              complete(ExerciseListResponse(exercises))
            }
          }
        }
      },
      post {
        path("exercises") {
          entity(as[Exercise]) { exercise =>
            sessionManagement.withRequiredSession { session: UserSession =>
              val future: Future[Option[Exercise]] =
                if (exercise.userId == session.userId) {
                  exerciseService.save(exercise).map(e => Some(e))
                } else {
                  Future.successful(None)
                }
              onSuccess(future) {
                case Some(exercise) => complete(exercise)
                case None           => complete(StatusCodes.Forbidden)
              }
            }
          }
        }
      }
    )
}
