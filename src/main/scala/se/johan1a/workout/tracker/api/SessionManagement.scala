package se.johan1a.workout.tracker.api

import org.apache.pekko.http.scaladsl.server.{Directive0, Directive1}
import com.softwaremill.pekkohttpsession.SessionDirectives.{
  invalidateSession,
  requiredSession,
  setSession
}
import com.softwaremill.pekkohttpsession.SessionOptions.{refreshable, usingCookies}
import com.softwaremill.pekkohttpsession.{SessionConfig, SessionManager}
import com.typesafe.config.Config
import se.johan1a.workout.tracker.auth.UserSession

import scala.concurrent.ExecutionContext
import com.softwaremill.pekkohttpsession.RefreshTokenStorage

class SessionManagement()(implicit
    refreshTokenStorage: RefreshTokenStorage[UserSession],
    config: Config,
    executionContext: ExecutionContext
) {

  private val serverSecret: String = config.getString("auth.serverSecret")

  private val sessionConfig: SessionConfig = SessionConfig.default(serverSecret)

  private implicit val sessionManager: SessionManager[UserSession] =
    new SessionManager[UserSession](sessionConfig)

  val withRequiredSession: Directive1[UserSession] =
    requiredSession(refreshable, usingCookies)
  val myInvalidateSession: Directive0 =
    invalidateSession(refreshable, usingCookies)

  def mySetSession(v: UserSession): Directive0 =
    setSession(refreshable, usingCookies, v)

}
