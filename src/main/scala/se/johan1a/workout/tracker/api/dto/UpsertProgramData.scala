package se.johan1a.workout.tracker.api.dto

import se.johan1a.workout.tracker.model.TrainingMax

import java.util.UUID

object OffsetMode {
  val ZERO = 0
  // Every other round, offset days by one.
  // To alternate between A - B - A and B - A - B
  val ONE = 1
}

case class UpsertProgramData(
    weeks: Seq[UpsertProgramWeek],
    trainingMaxes: Seq[TrainingMax],
    currentRound: Option[Int],
    currentDay: Option[Int],
    currentWeek: Option[Int],
    offsetMode: Int,
    createdWorkoutIds: Option[Seq[UUID]]
)
