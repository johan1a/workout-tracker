package se.johan1a.workout.tracker.api.dto

import se.johan1a.workout.tracker.model._

import java.util.UUID

case class UpsertProgramCommand(
    id: Option[UUID],
    programType: Int,
    name: String,
    data: UpsertProgramData
)

object UpsertProgramCommand {
  def fromCommand(command: UpsertProgramCommand, userId: UserId): Program = {
    val data = command.data
    Program(
      data = ProgramData(
        weeks = data.weeks.zipWithIndex.map { case (week: UpsertProgramWeek, weekIndex) =>
          ProgramWeek(
            index = week.index.getOrElse(weekIndex),
            days = week.days.zipWithIndex.map { case (day, dayIndex) =>
              ProgramDay(
                index = day.index.getOrElse(dayIndex),
                exercises = day.exercises.zipWithIndex.map { case (exercise, exerciseIndex) =>
                  ProgramExercise(
                    index = Some(exercise.index.getOrElse(exerciseIndex)),
                    exerciseId = exercise.exerciseId,
                    sets = exercise.sets.zipWithIndex.map { case (set, setIndex) =>
                      ProgramSet(
                        index = set.index.getOrElse(setIndex),
                        percentageOfMax = set.percentageOfMax,
                        fixedWeight = set.fixedWeight,
                        nbrReps = set.nbrReps
                      )
                    }
                  )
                }
              )
            }
          )
        },
        trainingMaxes = data.trainingMaxes,
        currentRound = data.currentRound.getOrElse(0),
        currentDay = data.currentDay.getOrElse(0),
        currentWeek = data.currentWeek.getOrElse(0),
        offsetMode = data.offsetMode,
        createdWorkoutIds = data.createdWorkoutIds.getOrElse(Seq.empty)
      ),
      programType = command.programType,
      id = command.id,
      name = command.name,
      userId = userId.value
    )
  }
}
