package se.johan1a.workout.tracker.api

import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import org.apache.pekko.http.scaladsl.server.Directives._
import se.johan1a.workout.tracker.api.JsonFormat._
import se.johan1a.workout.tracker.auth.UserSession
import se.johan1a.workout.tracker.model.{RecordData, RecordsResponse}
import se.johan1a.workout.tracker.service.{RecordsService, WorkoutService}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}

class RecordRoutes(
    recordsService: RecordsService,
    workoutService: WorkoutService,
    sessionManagement: SessionManagement
)(implicit executionContext: ExecutionContext) {

  val routes = concat(
    get {
      pathPrefix("records") {
        sessionManagement.withRequiredSession { session: UserSession =>
          val maybeResult: Future[RecordData] =
            recordsService.fetch(session.userId)

          onSuccess(maybeResult) { recordData: RecordData =>
            complete(RecordsResponse(recordData))
          }
        }
      }
    },
    post {
      pathPrefix("records" / "recalculate") {
        sessionManagement.withRequiredSession { session: UserSession =>
          recordsService
            .deleteRecords(session.userId)
            .map(_ =>
              workoutService
                .list(session.userId)
                .map { workouts =>
                  workouts.reverse
                    .foreach(workout => {
                      Await.result(
                        recordsService
                          .saveNewRecords(session.userId, workout),
                        30.seconds
                      )
                    })
                }
            )
          complete("OK")
        }
      }
    }
  )

}
