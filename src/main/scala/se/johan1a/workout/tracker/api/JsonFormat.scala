package se.johan1a.workout.tracker.api

import se.johan1a.workout.tracker.api.dto.{
  UpsertProgramCommand,
  UpsertProgramData,
  UpsertProgramDay,
  UpsertProgramExercise,
  UpsertProgramSet,
  UpsertProgramWeek
}
import se.johan1a.workout.tracker.model._
import se.johan1a.workout.tracker.model.request._
import spray.json.DefaultJsonProtocol._
import spray.json.{JsString, JsValue, JsonFormat, RootJsonFormat}

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

object JsonFormat {

  implicit val uuidFormat: JsonFormat[UUID] =
    new RootJsonFormat[UUID] {
      override def write(obj: UUID): JsValue = JsString(
        obj.toString
      )

      override def read(json: JsValue): UUID =
        json match {
          case JsString(str) => UUID.fromString(str)
          case _ =>
            throw new RuntimeException(s"Could not parse $json into UUID")
        }
    }

  implicit val localDateFormat: JsonFormat[LocalDate] =
    new RootJsonFormat[LocalDate] {
      override def write(obj: LocalDate): JsValue = JsString(
        obj.toString
      )

      override def read(json: JsValue): LocalDate =
        json match {
          case JsString(str) => LocalDate.parse(str)
          case _ =>
            throw new RuntimeException(
              s"Could not parse $json into LocalDate"
            )
        }
    }

  implicit val localDateTimeFormat: JsonFormat[LocalDateTime] =
    new RootJsonFormat[LocalDateTime] {
      override def write(obj: LocalDateTime): JsValue = JsString(
        obj.toString
      )

      override def read(json: JsValue): LocalDateTime =
        json match {
          case JsString(str) => LocalDateTime.parse(str)
          case _ =>
            throw new RuntimeException(
              s"Could not parse $json into LocalDateTime"
            )
        }

    }

  implicit val exerciseFormat: RootJsonFormat[Exercise] = jsonFormat4(
    Exercise.apply
  )

  implicit val recordMarkerFormat: JsonFormat[RecordMarker] = jsonFormat1(
    RecordMarker.apply
  )

  implicit val exerciseSetFormat: RootJsonFormat[ExerciseSet] = jsonFormat6(
    ExerciseSet.apply
  )

  implicit val exerciseListResponseFormat: RootJsonFormat[ExerciseListResponse] = jsonFormat1(
    ExerciseListResponse.apply
  )

  implicit val workoutExerciseFormat: RootJsonFormat[WorkoutExercise] =
    jsonFormat3(
      WorkoutExercise.apply
    )

  implicit val workoutDataFormat: RootJsonFormat[WorkoutData] = jsonFormat1(
    WorkoutData.apply
  )

  implicit val workoutFormat: RootJsonFormat[Workout] = jsonFormat6(
    Workout.apply
  )

  implicit val workoutListResponseFormat: RootJsonFormat[WorkoutListResponse] =
    jsonFormat1(
      WorkoutListResponse.apply
    )

  implicit val trainingMaxFormat: RootJsonFormat[TrainingMax] = jsonFormat3(
    TrainingMax.apply
  )

  implicit val programSetFormat: RootJsonFormat[ProgramSet] = jsonFormat4(
    ProgramSet.apply
  )

  implicit val programExerciseFormat: RootJsonFormat[ProgramExercise] =
    jsonFormat3(
      ProgramExercise.apply
    )

  implicit val programDayFormat: RootJsonFormat[ProgramDay] = jsonFormat2(
    ProgramDay.apply
  )

  implicit val programWeekFormat: RootJsonFormat[ProgramWeek] = jsonFormat2(
    ProgramWeek.apply
  )

  implicit val programDataFormat: RootJsonFormat[ProgramData] = jsonFormat7(
    ProgramData.apply
  )

  implicit val programFormat: RootJsonFormat[Program] = jsonFormat5(
    Program.apply
  )

  implicit val upsertProgramSetFormat: RootJsonFormat[UpsertProgramSet] = jsonFormat4(
    UpsertProgramSet.apply
  )

  implicit val upsertProgramExerciseFormat: RootJsonFormat[UpsertProgramExercise] = jsonFormat3(
    UpsertProgramExercise.apply
  )

  implicit val upsertProgramDayFormat: RootJsonFormat[UpsertProgramDay] = jsonFormat2(
    UpsertProgramDay.apply
  )

  implicit val upsertProgramWeekFormat: RootJsonFormat[UpsertProgramWeek] = jsonFormat2(
    UpsertProgramWeek.apply
  )

  implicit val upsertProgramDataFormat: RootJsonFormat[UpsertProgramData] = jsonFormat7(
    UpsertProgramData.apply
  )

  implicit val upsertProgramCommandFormat: RootJsonFormat[UpsertProgramCommand] = jsonFormat4(
    UpsertProgramCommand.apply
  )

  implicit val programListResponseFormat: RootJsonFormat[ProgramListResponse] =
    jsonFormat1(
      ProgramListResponse.apply
    )

  implicit val userResponseUserFormat: JsonFormat[UserResponseUser] =
    jsonFormat2(
      UserResponseUser.apply
    )

  implicit val userResponseFormat: RootJsonFormat[UserResponse] = jsonFormat1(
    UserResponse.apply
  )

  implicit val loginRequestFormat: RootJsonFormat[LoginRequest] = jsonFormat2(
    LoginRequest.apply
  )

  implicit val createUserRequestFormat: RootJsonFormat[CreateUserRequest] =
    jsonFormat2(
      CreateUserRequest.apply
    )

  implicit val repMaxFormat: JsonFormat[RepMax] =
    jsonFormat4(
      RepMax.apply
    )

  implicit val exerciseRecordDataFormat: JsonFormat[ExerciseRecordData] =
    jsonFormat2(
      ExerciseRecordData.apply
    )

  implicit val recordDataFormat: JsonFormat[RecordData] =
    jsonFormat2(
      RecordData.apply
    )

  implicit val recordsResponseFormat: RootJsonFormat[RecordsResponse] =
    jsonFormat1(
      RecordsResponse.apply
    )

  implicit val createWorkoutMethod: RootJsonFormat[CreateWorkoutMethod] =
    new RootJsonFormat[CreateWorkoutMethod] {
      override def write(obj: CreateWorkoutMethod): JsValue = JsString(
        obj.toString
      )

      override def read(json: JsValue): CreateWorkoutMethod =
        json match {
          case JsString(str) =>
            str match {
              case "FromProgram" => FromProgram
              case "CopyWorkout" => CopyWorkout
              case "New"         => New
            }
          case _ =>
            throw new RuntimeException(
              s"Could not parse $json into CreateWorkoutMethod"
            )
        }
    }

  implicit val createWorkoutRequest: RootJsonFormat[CreateWorkoutRequest] =
    jsonFormat4(CreateWorkoutRequest.apply)

  implicit val userIdJsonFormat: RootJsonFormat[UserId] =
    new RootJsonFormat[UserId] {
      override def read(json: JsValue): UserId = {
        json match {
          case JsString(id) => UserId.apply(UUID.fromString(id))
          case _ =>
            throw new Exception(s"Unexpected json type for MetricTypeId: $json")
        }
      }
      override def write(obj: UserId): JsValue = JsString(
        obj.value.toString
      )
    }

  // TODO refactor
  implicit val metricTypeIdJsonFormat: RootJsonFormat[MetricTypeId] =
    new RootJsonFormat[MetricTypeId] {
      override def read(json: JsValue): MetricTypeId = {
        json match {
          case JsString(id) => MetricTypeId.apply(UUID.fromString(id))
          case _ =>
            throw new Exception(s"Unexpected json type for MetricTypeId: $json")
        }
      }

      override def write(obj: MetricTypeId): JsValue = JsString(
        obj.value.toString
      )
    }

  implicit val metricTypeNameJsonFormat: RootJsonFormat[MetricTypeName] =
    new RootJsonFormat[MetricTypeName] {
      override def read(json: JsValue): MetricTypeName =
        json match {
          case JsString(id) => MetricTypeName.apply(id)
          case _ =>
            throw new Exception(
              s"Unexpected json type for MetricTypeName: $json"
            )
        }

      override def write(obj: MetricTypeName): JsValue = JsString(
        obj.value
      )
    }

  implicit val metricJsonFormat: RootJsonFormat[Metric] = jsonFormat4(
    Metric.apply
  )

  implicit val metricTypeJsonFormat: RootJsonFormat[MetricType] = jsonFormat5(
    MetricType.apply
  )
}
