package se.johan1a.workout.tracker.api.dto

case class UpsertProgramWeek(index: Option[Int], days: Seq[UpsertProgramDay])
