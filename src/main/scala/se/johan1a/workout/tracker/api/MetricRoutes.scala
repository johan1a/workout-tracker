package se.johan1a.workout.tracker.api

import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import com.typesafe.scalalogging.Logger
import se.johan1a.workout.tracker.api.JsonFormat.{metricJsonFormat, metricTypeJsonFormat}
import se.johan1a.workout.tracker.auth.UserSession
import se.johan1a.workout.tracker.exception.NotFoundException
import se.johan1a.workout.tracker.model.{Metric, MetricType, MetricTypeId, UserId}
import se.johan1a.workout.tracker.service.{MetricService, MetricTypeService}
import spray.json.DefaultJsonProtocol._

import java.util.UUID
import scala.concurrent.Future
import scala.util._

class MetricRoutes(
    sessionManagement: SessionManagement,
    metricTypeService: MetricTypeService,
    metricService: MetricService
)(implicit logger: Logger) {

  val routes: Route = {
    pathPrefix("metric-types") {
      concat(
        path(JavaUUID / "metrics") { metricTypeId =>
          concat(
            listMetrics(metricTypeId),
            upsertMetric,
            deleteMetric
          )
        },
        path(JavaUUID) { metricTypeId =>
          concat(
            getMetricType(metricTypeId),
            deleteMetricType(metricTypeId)
          )
        },
        upsertMetricType,
        listMetricTypes
      )
    }
  }

  private def listMetricTypes: Route = {
    get {
      sessionManagement.withRequiredSession { session: UserSession =>
        val metricTypeFuture: Future[Seq[MetricType]] =
          metricTypeService.list(
            UserId(session.userId)
          )
        onComplete(metricTypeFuture) {
          case Success(metricTypes) =>
            complete(StatusCodes.OK, metricTypes)
          case Failure(exception) =>
            logger.warn(
              "Got Exception when listing MetricTypes: ",
              exception
            )
            complete(StatusCodes.NotFound)
        }
      }
    }
  }

  private def deleteMetricType(metricTypeId: UUID): Route = {
    delete {
      sessionManagement.withRequiredSession { session: UserSession =>
        val metricTypeFuture: Future[Unit] =
          metricTypeService.delete(
            UserId(session.userId),
            MetricTypeId(metricTypeId)
          )
        onComplete(metricTypeFuture) {
          case Success(_) =>
            complete(StatusCodes.OK)
          case Failure(exception) =>
            logger.error("Got Exception when deleting MetricType: ", exception)
            complete(StatusCodes.BadRequest)
        }
      }
    }
  }

  private def upsertMetricType: Route = {
    post {
      entity(as[MetricType]) { (request: MetricType) =>
        sessionManagement.withRequiredSession { session: UserSession =>
          val metricTypeFuture: Future[MetricType] =
            metricTypeService.upsert(UserId(session.userId), request)
          onComplete(metricTypeFuture) {
            case Success(metricType: MetricType) =>
              complete(StatusCodes.OK, metricType)
            case Failure(exception) =>
              logger.warn(
                "Got Exception when upserting MetricType: ",
                exception
              )
              complete(StatusCodes.BadRequest)
          }
        }
      }
    }
  }

  private def getMetricType(metricTypeId: UUID): Route = {
    get {
      sessionManagement.withRequiredSession { session: UserSession =>
        val metricTypeFuture: Future[Option[MetricType]] =
          metricTypeService.fetch(UserId(session.userId), MetricTypeId(metricTypeId))
        onComplete(metricTypeFuture) {
          case Success(Some(metricType: MetricType)) =>
            complete(StatusCodes.OK, metricType)
          case Success(None) =>
            complete(StatusCodes.NotFound)
          case Failure(exception) =>
            logger.error(
              "Got Exception when fetching MetricType: ",
              exception
            )
            complete(StatusCodes.BadRequest)
        }
      }
    }
  }

  private val upsertMetric: Route = {
    post {
      entity(as[Metric]) { (request: Metric) =>
        sessionManagement.withRequiredSession { session: UserSession =>
          val metricFuture: Future[Metric] =
            metricService.upsert(UserId(session.userId), request)
          onComplete(metricFuture) {
            case Success(metric: Metric) =>
              complete(StatusCodes.OK, metric)
            case Failure(exception) =>
              logger.error(
                "Got Exception when upserting Metric: ",
                exception
              )
              complete(StatusCodes.BadRequest)
          }
        }
      }
    }
  }

  private val deleteMetric: Route = {
    delete {
      entity(as[Metric]) { (request: Metric) =>
        sessionManagement.withRequiredSession { session: UserSession =>
          val metricFuture: Future[Unit] =
            metricService.delete(UserId(session.userId), request.metricTypeId, request.date)
          onComplete(metricFuture) {
            case Success(()) => complete(StatusCodes.OK)
            case Failure(exception) =>
              logger.error(
                "Got Exception when upserting Metric: ",
                exception
              )
              complete(StatusCodes.BadRequest)
          }
        }
      }
    }
  }

  private def listMetrics(metricTypeId: UUID): Route = {
    get {
      sessionManagement.withRequiredSession { session: UserSession =>
        val metricsFuture: Future[Seq[Metric]] =
          metricService.list(UserId(session.userId), MetricTypeId(metricTypeId))
        onComplete(metricsFuture) {
          case Success(metrics: Seq[Metric]) =>
            complete(StatusCodes.OK, metrics)
          case Failure(exception) =>
            exception match {
              case NotFoundException(_) => complete(StatusCodes.NotFound)
              case e =>
                logger.error("Got Exception when listing metrics: ", e)
                complete(StatusCodes.NotFound)
            }
        }
      }
    }
  }
}
