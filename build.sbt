val scala2Version = "2.13.16"

val akkaVersion = "1.1.3"
val akkaHttpVersion = "1.1.0"
val testcontainersScalaVersion = "0.41.4"
val slickVersion = "3.4.1"
val akkaHttpSessionVersion = "0.7.1"

val dependencies = Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
  "ch.qos.logback" % "logback-classic" % "1.5.16",
  "org.apache.pekko" %% "pekko-actor-typed" % akkaVersion,
  "org.apache.pekko" %% "pekko-stream" % akkaVersion,
  "org.apache.pekko" %% "pekko-http" % akkaHttpVersion,
  "org.apache.pekko" %% "pekko-http-spray-json" % akkaHttpVersion,
  "com.typesafe.slick" %% "slick" % slickVersion,
  "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
  "org.postgresql" % "postgresql" % "42.7.5",
  "com.softwaremill.pekko-http-session" %% "core" % akkaHttpSessionVersion,
  "com.softwaremill.pekko-http-session" %% "jwt" % akkaHttpSessionVersion,
  "org.json4s" %% "json4s-ext" % "4.1.0-M8",
  "com.github.t3hnar" %% "scala-bcrypt" % "4.3.0",
  "org.liquibase" % "liquibase-core" % "4.31.1",
  "com.mattbertolini" % "liquibase-slf4j" % "5.1.0",
  "org.scalatest" %% "scalatest" % "3.2.19" % Test,
  "org.scalamock" %% "scalamock" % "6.1.1" % Test,
  "org.scalatestplus" %% "scalacheck-1-15" % "3.2.11.0" % Test,
  "org.apache.pekko" %% "pekko-stream-testkit" % akkaVersion % Test,
  "org.apache.pekko" %% "pekko-http-testkit" % akkaHttpVersion % Test,
  "com.dimafeng" %% "testcontainers-scala-scalatest" % testcontainersScalaVersion % Test,
  "com.dimafeng" %% "testcontainers-scala-mysql" % testcontainersScalaVersion % Test,
  "com.dimafeng" %% "testcontainers-scala-postgresql" % testcontainersScalaVersion % Test
)

lazy val root = project
  .in(file("."))
  .settings(
    name := "workout-tracker",
    organization := "se.johan1a",
    version := "0.1.0",
    scalaVersion := scala2Version,
    libraryDependencies ++= dependencies
  )

assembly / assemblyJarName := "workout-tracker.jar"

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", xs @ _*) =>
    (xs map { _.toLowerCase }) match {
      case "services" :: xs =>
        MergeStrategy.filterDistinctLines
      case _ => MergeStrategy.discard
    }
  case PathList("reference.conf") => MergeStrategy.concat
  case _                          => MergeStrategy.first
}

(Test / test) := ((Test / test) dependsOn (Compile / scalafmtCheck)).value

scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-Xfatal-warnings",
  "-Wunused:implicits",
  "-Wunused:explicits",
  "-Wunused:imports",
  "-Wunused:locals",
  "-Wunused:params",
  "-Wunused:privates"
)
