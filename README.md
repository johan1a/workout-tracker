# Workout Tracker


## Starting

```
sbt ~reStart
```

## Run tests

```
# Run all tests
sbt test

# Run a single test
sbt 'test:testOnly *Record*'
```

## Log in locally

```
# Login
http --auth admin:admin --session local POST :8080/api/auth/login

# Authenticated request
http --session local  :8080/api/auth/user
```
