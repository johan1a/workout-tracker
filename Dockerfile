FROM eclipse-temurin:19-jre-focal

WORKDIR /app

ENV JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF-8

COPY target/scala-*/workout-tracker.jar /app/bin/workout-tracker

EXPOSE 8080
CMD java -jar /app/bin/workout-tracker -Dhttp.port=8080
