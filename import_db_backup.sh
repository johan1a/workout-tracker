# On server
docker exec workout-tracker-postgres-1 pg_dump -U workout_tracker workout_tracker -h localhost > workout_tracker.sql

# On client
rsync urdatorn2:workout_tracker.sql ./
psql -h localhost -p 6432 -U workout_tracker workout_tracker < workout_tracker.sql

