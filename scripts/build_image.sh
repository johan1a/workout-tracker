#!/bin/sh
GIT_SHA=$(git rev-parse HEAD)
export JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF-8' && sbt assembly
docker pull johan1a/workout-tracker:latest || true
docker build --cache-from johan1a/workout-tracker:latest -t johan1a/workout-tracker:latest -t johan1a/workout-tracker:${GIT_SHA} .
